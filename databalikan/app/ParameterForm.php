<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class ParameterForm extends Model
{
   public $fillable = ['id_lembaga','parameter'];
}
