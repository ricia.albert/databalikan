<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Auth;
use File;
use Carbon\Carbon;
use App\Jobs\InsertData;
use Regulus\ActivityLog\Models\Activity;
use Illuminate\Support\Facades\Session;


class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $search = $request->input('s');
        $search = ($search == null) ? "" : $search;
        $lembaga = DB::table('users')->join("setup_lembaga","setup_lembaga.id_lembaga","=","users.id_lembaga")->where("users.id_lembaga","<>","ADMIN")->where("datacount",">",0)->where(\DB::raw("LOWER(email)"),"like","%".strtolower($search)."%")->orderBy("users.id_lembaga","asc")->get();
        $total = DB::table('users')->where("id_lembaga","<>","ADMIN")->where("email","like","%".$search."%")->sum("datacount");
        return view('report', ['lembaga' => $lembaga, 'search' => $search, 'total' => $total]);
    }
}
