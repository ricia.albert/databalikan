<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Auth;
use File;
use Response;
use Carbon\Carbon;
use App\Jobs\InsertData;
use Regulus\ActivityLog\Models\Activity;
use Illuminate\Support\Facades\Session;


class UploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
            return abort(404);

        $namaLembaga = DB::table('setup_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->first();

        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->get();
        if($prevParameter->count()<=1){
            return redirect('setParameter');
        }

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' open form upload untuk '.Auth::user()->id_lembaga,'action' => 'View'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('upload/importExport',['nama'=>$namaLembaga->lembaga_pengguna]);
    }

    public function sendfile(Request $request)
    {
        if(Input::hasFile('fnupload')){
            $file = $request->file('fnupload');
            $destinationPath = './storage/uploads/';
            $newFileName = Carbon::now()->format('Y-m-d').'_'.str_random(8).'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $newFileName);

             // $path = Input::file('fnupload')->getRealPath();
            $data = Excel::load($destinationPath.'/'.$newFileName, function($reader) {
                $reader->noHeading();
                $reader->limitRows(800000);
            })->get();

            $prop = array();
            $idxNIK = 0;
            if($data->count() <= 800000){
                if(!empty($data) && $data->count()){
                    foreach ($data as $i => $value){
                        $noKtp="";
                        $date = date_create();
                        $time = date_timestamp_get($date);
                        $row = $value->toArray();
                        $insert = array();

                        if ($i == 0) {
                            // Mapping Column
                            for ($j = 0; $j < sizeof($row); $j++) {
                                $prop[$j] = $row[$j];
                                if ($row[$j] == 'NIK') {
                                    $idxNIK = $j;
                                }
                            }
                        } else {
                            for ($j = 0; $j < sizeof($row); $j++) {
                                if ($j != $idxNIK) {
                                    $date = date_create();
                                    $time = date_timestamp_get($date);
                                    $insert[] = ['id_lembaga' => Auth::user()->id_lembaga, 'parameter' => $prop[$j], 'value' => $row[$j], 'nik' => $row[$idxNIK], 'time'=> $time,'source'=>2, 'tgl_import' => date('Y-m-d H:i')];
                                    DB::table('data_balikan')->insert($insert);
                                    unset($insert);
                                }
                            }
                        }
                    }
                }
            }

            redirect('upload?scs=1');
        } else
            redirect('upload?err=1');
    }

    public function sendcsv(Request $request)
    {
        if(Input::hasFile('fnupload')){
            $file = $request->file('fnupload');
            $destinationPath = './storage/uploads/';
            $newFileName = Carbon::now()->format('Y-m-d').'_'.str_random(8).'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $newFileName);

            // Baca CSV File
            $file = fopen($destinationPath."/".$newFileName,"r");
            $i = 0; 
            $prop = array();
            $idxNIK = 0;
            while (!feof($file)) {
                $line = fgets($file);
                // echo $line."<br/>";
                
                if ($line != "") {
                   
                    $noKtp="";
                    $date = date_create();
                    $time = date_timestamp_get($date);
                    $row = explode("|", $line);
                    $insert = array();

                    if ($i == 0) {
                        // Mapping Column
                        for ($j = 0; $j < sizeof($row); $j++) {
                            $prop[$j] = $row[$j];
                            if ($row[$j] == 'NIK') {
                                $idxNIK = $j;
                            }
                        }
                    } else {
                        for ($j = 0; $j < sizeof($row); $j++) {
                            if ($j != $idxNIK) {
                                $date = date_create();
                                $time = date_timestamp_get($date);
                                $insert[] = ['id_lembaga' => Auth::user()->id_lembaga, 'parameter' => $prop[$j], 'value' => $row[$j], 'nik' => $row[$idxNIK], 'time'=> $time,'source'=>2, 'tgl_import' => date('Y-m-d H:i')];
                                DB::table('data_balikan')->insert($insert);
                                unset($insert);
                            }
                        }
                    }
                    $i++;
                }

            }

            Redirect::action('UploadController@index', array('scs' => 1));
        } else
            Redirect::action('UploadController@index', array('err' => 1));
    }

    public function uploadFile(Request $request)
    {
        if(Input::hasFile('import_file')){
            $file = $request->file('import_file');
            $destinationPath = storage_path().'/uploads/';
            $newFileName = Carbon::now()->format('Y-m-d').'_'.str_random(8).'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $newFileName);

            DB::table('users')->where('id_lembaga', Auth::user()->id_lembaga)->update(['last_send' => Carbon::now()->format('Y-m-d H:i')]);

            Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'import_file' ,'details'=> Auth::user()->email.' melakukan upload file untuk '.Auth::user()->id_lembaga,'updated' => 0
                ,'developer'=>0,'language_key'=>0,'public'=>0]);

            $this->dispatch((new InsertData($newFileName,Auth::user()->id_lembaga,Auth::user()->id,Auth::user()->email))->delay(now()->addMinutes(5)));

            Session::flash('flash_message', 'Data excel berhasil diupload. proses import data akan dilakukan 5 menit kemudian');
            return back();
        }
        Session::flash('flash_message', 'Masukkan file yang akan diupload');
        return back();
    }
    public function importExcel()
    {
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
                $reader->noHeading();
                $reader->limitRows(800000);
            })->get();

            Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'import_file' ,'details'=> Auth::user()->email.' melakukan import file untuk '.Auth::user()->id_lembaga,'updated' => 0
                ,'developer'=>0,'language_key'=>0,'public'=>0]);

            ini_set('max_execution_time', 72000);
            if($data->count() <= 800000){
                if(!empty($data) && $data->count()){
                    foreach ($data as $i => $value){
                        $noKtp="";
                        $date = date_create();
                        $time = date_timestamp_get($date);
                        if($i == 0){
                            $arrayKeys = $value;
                        }
                        if($i > 0){
                            $insert = array();
                            foreach (array_chunk($value->toArray(),10000) as $item) {
                                $j=0;
                                foreach ($item as $arrayKey=>$arrayValue) {
                                    if($j==0){
                                        $noKtp = $arrayValue;
                                    }
                                    if($arrayValue!=null) {
                                        $insert[] = ['id_lembaga' => Auth::user()->id_lembaga, 'parameter' => $arrayKeys[$arrayKey], 'value' => $arrayValue, 'nik' => $noKtp, 'time'=> $time,'source'=>2];
                                        $j++;
                                    }
                                }
                            }
                            DB::table('data_balikan')->insert($insert);
                            unset($insert);
                        }
                    }
                    $totalRows = sizeof($data);
                    DB::table('users')->increment('datacount', $totalRows);
                    if(!empty($insert)){
                        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'import_file' ,'details'=> Auth::user()->email.' melakukan import file untuk '.Auth::user()->id_lembaga.' , jumlah baris : '.($totalRows  - 1),'updated' => 0
                            ,'developer'=>0,'language_key'=>0,'public'=>0]);
                    }
                    Session::flash('flash_message', 'Data excel berhasil diupload, total rows : '.($totalRows));
                    return back();
                }
            } else {
                Session::flash('flash_message', 'Data excel gagal diupload, total baris : '.($data->count()).' dari maks. 80000 ');
                return back();
            }
        }

        Session::flash('flash_message', 'Masukkan file yang akan diupload');
        return back();
    }

    public function downloadExcel($type)
    {
        $title = "UPLOAD DATA BALIKAN";
        $dirjen = "DIREKTORAT JENDERAL KEPENDUDUKAN DAN PENCATATAN SIPIL";
        $mendagri = "KEMENTERIAN DALAM NEGERI";

        $headers = "Kode = ".Auth::user()->id_lembaga;
        $note = "NOTE :";
        $note_1 = "1. DILARANG MENAMBAH KOLOM ATAU MENGUBAH HEADER DATA BALIKAN DI ROW KE-12";
        $note_2 = "2. MAKSIMUM ISI 1X UPLOAD SEBANYAK 500.000 ROW";
        $note_3 = "3. DATA AKAN DIAMBIL MULAI ROW KE-13";

        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->orderBy("indexno", "ASC")->get();
        $index =0;
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' melakukan download form excel untuk '.Auth::user()->id_lembaga,'action' => 'Download'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        if($prevParameter->count()<=0){
            return redirect('setParameter');
        }
        return Excel::create('format_upload', function($excel) use ($prevParameter) {
            $excel->sheet('mySheet', function($sheet) use ($prevParameter)
            {
                /*$sheet->row(1, array($title));
                $sheet->row(2,array($dirjen));
                $sheet->row(3,array($mendagri));
                $sheet->row(5,array($headers));
                $sheet->row(7,array($note));
                $sheet->row(8,array($note_1));
                $sheet->row(9,array($note_2));
                $sheet->row(10,array($note_3));*/
                foreach ($prevParameter as $key => $value) {
                    $val[] =$value->parameter;
                }
                $sheet->row(1, $val);
            });
        })->download($type);
    }

    public function downloadCSV($type)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $reviews = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->orderBy("indexno", "ASC")->get();

        $callback = function() use ($reviews)
        {
            $format_upload = fopen('php://output', 'w');
            $row = array();
            foreach($reviews as $review) {
               $row[] = $review->parameter;
            }
            fputcsv($format_upload, $row, "|");
            fclose($format_upload);
        };
        return Response::stream($callback, 200, $headers);
    }
   
}
