<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Regulus\ActivityLog\Models\Activity;
use Auth;
use Validator;
use App\User;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $result = DB::table('slider')->orderBy("id_slider", "ASC")->get();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'slider','description' => 'setup_slider' ,'details'=> Auth::user()->email.' view list slider','action' => 'List'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        return view('slider.list',compact('result'));
    }
    public function create(Request $request)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'slider','description' => 'setup_slider' ,'details'=> Auth::user()->email.' open form add data slider ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('slider.add');
    }
    public function edit(Request $request)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'slider','description' => 'setup_slider' ,'details'=> Auth::user()->email.' open form add data slider ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        $id = $request->input("id");
        $detail = DB::table('slider')->where('id_slider',$id)->first();
        return view('slider.edit',["detail" => $detail]);
    }
    protected function tambah(Request $request)
    {
        $obj = DB::table("slider")->select(\DB::raw('MAX(id_slider) as latest_id'))->first();
        $id_slider = 1;
        if ($obj != null)
            $id_slider = $obj->latest_id + 1;

        $file = $request->file('fnimage');
        $filename = time().".jpg";
        $file->move("images", $filename);

        DB::table("slider")->insert([
            'id_slider' => $id_slider,
            'slider_path' => $filename,
            'tgl_update'=> date('Y-m-d H:i')
        ]);
        return redirect("/listslider");

    }
    protected function delete(Request $request)
    {
        $id_slider = $request->input('id');
        DB::table('slider')->where('id_slider', '=', $id_slider)->delete();

        return redirect('listslider');
    }
    public function update(Request $request)
    {
        $id_slider = $request->input('id');

        $file = $request->file('fnimage');
        $filename = time().".jpg";
        $file->move("images", $filename);

        DB::table("slider")->where("id_slider", "=", $id_slider)->update(["slider_path" => $filename]);

        return redirect('/listslider');
    }
}
