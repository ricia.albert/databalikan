<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Regulus\ActivityLog\Models\Activity;
use Auth;
use DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('users')->where("id_lembaga","=",Auth::user()->id_lembaga)->first();

        // Current Data
        $sd = date('Y-m-d');
        $ed = date('Y-m-d', strtotime('+1days'));

        $sliders = DB::table('slider')->orderBy("ID_slider", "asc")->get();
        $current = DB::table('data_balikan')->whereBetween('tgl_import', [date('Y-m-d', strtotime($sd)), date('Y-m-d', strtotime($ed))])->join('users', 'data_balikan.id_lembaga', '=', 'users.id_lembaga')->where("users.id_lembaga","=",Auth::user()->id_lembaga)->groupBy('users.name','email')->select(\DB::raw('users.name, users.email, count(*) as datacount'))->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Access','description' => 'login' ,'details'=> Auth::user()->email.' melakukan login ke data balikan ','action' => 'Login', 'data' => $data
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('home', ["data" => $data, "current" => $current, "sliders" => $sliders]);
    }

    public function signout()
    {
        Session::flush();
        return redirect('login');
    }
}
