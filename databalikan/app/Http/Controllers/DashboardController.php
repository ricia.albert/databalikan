<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Auth;
use File;
use Carbon\Carbon;
use App\Jobs\InsertData;
use Regulus\ActivityLog\Models\Activity;
use Illuminate\Support\Facades\Session;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if (Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
            return abort(404);

        $search = $request->input('s');
        $search = ($search == null) ? "" : $search;
        $lembaga = DB::table('users')->where("id_lembaga","<>","ADMIN")->where(\DB::raw("LOWER(email)"),"LIKE","%".strtolower($search)."%")->orderBy("datacount","desc")->where("datacount",">",0)->get();
        $totalLembaga = DB::table('users')->join("setup_lembaga","setup_lembaga.id_lembaga","=","users.id_lembaga")->where(\DB::raw("LOWER(email)"),"LIKE","%".strtolower($search)."%")->where("users.id_lembaga","<>","ADMIN")->where("datacount",">",0)->count("users.id_lembaga");
        $total = DB::table('users')->where(\DB::raw("LOWER(email)"),"LIKE","%".strtolower($search)."%")->sum("datacount");
        return view('dashboard', ['lembaga' => $lembaga,'total' => $total, 'totalLembaga' => $totalLembaga, 'search' => $search]);
    }   
}
