<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Regulus\ActivityLog\Models\Activity;
use Auth;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('auth.activity');
    }
    public function getactivity(Request $request)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Activity Log','description' => 'activity_log' ,'details'=> Auth::user()->email.' view list activity log ','action' => 'List'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        // $result = DB::table('activity_log')->join('users','activity_log.user_id','users.id')
        //     ->select([DB::raw('row_number() OVER (ORDER BY activity_log.created_at) as nomor'),'activity_log.id','user_id','name','content_type','action','description','details','ip_address','activity_log.created_at'])
        //     ->orderBy('activity_log.created_at','desc'); //,'activity_log.updated_at'

        $result = DB::select("select row_number() OVER (ORDER BY created_at) as nomor,id,user_id,name,content_type,action,description,details,ip_address,created_at from 
                            (
                                select l.*, u.name from activity_log l
                                JOIN users u ON l.user_id = u.id
                                order by l.created_at desc
                            ) a");

        return Datatables::of($result)->make(true);
    }
    public function listdatabalikan(Request $request)
    {
        $filter = "";
        $result = DB::select($filter);

        return view('lembaga/list_balikan',compact('result'));
    }
}
