<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Regulus\ActivityLog\Models\Activity;
use Auth;
use Validator;
use App\User;

class ParamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $result = DB::table('setup_lembaga')->orderBy("id_lembaga", "ASC")->get();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' view list lembaga','action' => 'List'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        return view('lembaga.list',compact('result'));
    }
    public function detail($id)
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $detail = DB::table('setup_lembaga')->where('id_lembaga',$id)->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' view data lembaga '.$id,'action' => 'View'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        return view('lembaga.detail',compact('detail'));
    }
    public function create(Request $request)
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $jenis = DB::table("master_jenis_lembaga")->get();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' open form add data lembaga ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('lembaga.add',["jenis" => $jenis]);
    }
    public function edit(Request $request)
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);
            
        $jenis = DB::table("master_jenis_lembaga")->get();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' open form add data lembaga ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        $id = $request->input("id");
        $detail = DB::table('setup_lembaga')->where('id_lembaga',$id)->first();
        return view('lembaga.edit',["jenis" => $jenis, "detail" => $detail]);
    }
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'nama_lembaga' => 'required|string|max:255|unique:users',
        ]);
        #'email' => 'required|string|email|max:255|unique:users',
        #'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@]).*$/|min:8|confirmed',
    }
    protected function tambah($no_lembaga, array $data)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'users' ,'details'=> Auth::user()->email.' tambah data users '.json_encode($data) ,'updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return User::create([
            'name' => $no_lembaga,
            'email' => $data['email'],
            'id_lembaga'=>$no_lembaga,
            'password' => bcrypt($data['password']),
        ]);
    }
    protected function delete(Request $request)
    {
        $id_lembaga = $request->input('id');
        DB::table('setup_lembaga')->where('id_lembaga', '=', $id_lembaga)->delete();

        return redirect('listlembaga');
    }
    public function store(Request $request)
    {
        $lembaga = DB::table('setup_lembaga')->whereNotNull('no_lembaga')->orderBy(\DB::raw('TO_NUMBER(no_lembaga)'),'DESC')->first();
        $id_lembaga = str_pad(($lembaga->no_lembaga + 1), 4, "0", STR_PAD_LEFT);
        $jenis = str_pad($request['jenis_lembaga'], 2, "0", STR_PAD_LEFT);
        $no_lembaga = $jenis.$id_lembaga;

        //echo $no_lembaga;
        // $this->validator($request->all())->validate();
        $this->tambah($no_lembaga, $request->all());

        // $newid = DB::table('setup_lembaga')->max("id_lembaga");
        // $newid++;
        $data_array = array('lembaga_pengguna'=>$request->input('email'),'id_lembaga'=>$no_lembaga,
            'jenis_lembaga'=>$request->input('hid_jenis'), "no_lembaga" => ($lembaga->no_lembaga + 1));

        DB::table('setup_lembaga')->insert($data_array);

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' tambah data lembaga '.json_encode($data_array) ,'updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        Session::flash('flash_message', 'Data lembaga berhasil disimpan!');
        return redirect('listlembaga');
    }
    public function reset($id_lembaga)
    {
       $detail = User::where('id',$id_lembaga)->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Reset Password','description' => 'users' ,'details'=> Auth::user()->email.' buka form reset password ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('lembaga.reset',compact('detail'));

    }
    public function update(Request $request)
    {
        $request->validate([
            'password' => 'required|min:8|confirmed',
        ]);

        if($request->input('id_lembaga') != Auth::user()->id_lembaga){
            User::where('id_lembaga',$request->input('id_lembaga'))->update(['password'=>bcrypt($request->input('password'))]);
        }else{
            User::where('id',Auth::user()->id)->update(['password'=>bcrypt($request->input('password'))]);
        }

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Reset Password','description' => 'users' ,'details'=> Auth::user()->email.' melakukan reset password ','updated' => 1
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        Session::flash('flash_message', 'Password berhasil diubah!');
        return redirect('/');
    }

     public function modify(Request $request)
    {
        DB::table("setup_lembaga")->where("id_lembaga", $request->input("id"))->update(['lembaga_pengguna' => $request->input("nama_lembaga"), 'jenis_lembaga' => $request->input("hid_jenis")]);
        Session::flash('flash_message', 'Data berhasil diubah!');

        return redirect('listlembaga');
    }

    public function lembaga_balikan()
    {
        $result = "";

        return view('lembaga.list_balikan', compact($result));
    }

}
