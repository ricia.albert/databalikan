<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use Regulus\ActivityLog\Models\Activity;

class APIController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nik' => 'required',
            'id_lembaga'=>'required',
            'nama_lembaga'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(),404);
        }
        $noKtp = $request->input('nik');
        $id_lembaga = $request->input('id_lembaga');
        $data = $request->input('param');
        $email = $request->input('nama_lembaga');

        $x = explode(' ',$request->header('authorization'));
        if (sizeof($x) > 1) {
            $lembaga = User::where('id_lembaga',$id_lembaga)->where('api_token',$x[1])->first();
            if (empty($lembaga)) {
                return response()->json('ID Lembaga not available !',404);
            }
        } else {
            return response()->json('API Token Invalid !',404);
        }

        #ACTIVITY LOG UNTUK LOGIN API
        Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'users' ,'details'=> $email.' trial login to api dengan kode '.$id_lembaga ,'action' => 'Access API'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        $date = date_create();
        $time = date_timestamp_get($date);

        $dataObj = json_decode($data,true);
        foreach ($dataObj[0] as $arrayKey=>$arrayValue) {
            $review = DB::table('data_balikan')->where('parameter',$arrayKey)->where('value',$arrayValue)->where('nik',$noKtp)->where('id_lembaga', $id_lembaga)->first();
            if(!$review){
                if ($arrayKey != "NIK" && $arrayKey != "noKTP") {
                    $insert[] = array('id_lembaga' => $id_lembaga, 'parameter' => strtoupper($arrayKey), 'value' => strtoupper($arrayValue), 'nik' => $noKtp, 'source'=>3, 'tgl_import'=> date('Y-m-d H:i:s'));
                }
            }
        }
        if(!empty($insert)){
            $success = DB::table('data_balikan')->insert($insert);
            if($success){
                DB::commit();
            } else {
                DB::rollback();
            }
            #ACTIVITY LOG UNTUK POST DETAIL LEMBAGA
            Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'data_balikan' ,'details'=> $email.' insert data data_balikan '.$data ,'updated' => 0
                ,'developer'=>0,'language_key'=>0,'public'=>0]);

            return response()->json("Success",200);
        } else {
            return response()->json("Data sudah pernah dikirim",400);
        }
    }
    public function nik_store(Request $request)
    {
        $input = json_decode($request->getContent(),true);

        $id_lembaga = $input[0]['id_lembaga'];
        $data = $input[0]['data'];
        $email = $input[0]['nama_lembaga'];

        Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'users' ,'details'=> $email.' trial login to api dengan kode '.$id_lembaga ,'action' => 'Access API'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        $date = date_create();
        $time = date_timestamp_get($date);
        //$dataObj = json_decode($data,true);

        foreach ($data as $e)
        {
            for($i=0;$i<count($e['param']);$i++)
            {
                foreach ($e['param'][$i] as $j=>$k)
                {
                    $review = DB::table('data_balikan')->where('parameter',$j)->where('value',$k)->where('nik',$e['NIK'])->where('id_lembaga', $id_lembaga)->first();
                    if(!$review){
                        $insert[] = array('id_lembaga' => $id_lembaga, 'parameter' => $j, 'value' => strtoupper($k), 'nik'=>$e['NIK'], 'source'=>3, 'tgl_import'=> date('Y-m-d H:i:s'));
                    }
                }
            }
        }

        if(!empty($insert)){
            $success = DB::table('data_balikan')->insert($insert);
            if($success){
                DB::commit();
            } else {
                DB::rollback();
            }
            #ACTIVITY LOG UNTUK POST DETAIL LEMBAGA
            Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'data_balikan' ,'details'=> $email.' insert data data_balikan, jumlah rows '.count($insert) ,'updated' => 0
                ,'developer'=>0,'language_key'=>0,'public'=>0]);

            return response()->json("Success",200);
        }else{
            return response()->json("Data sudah pernah dikirim",400);
        }

    }
    public function getdetail(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nik' => 'required',
            'id_lembaga'=>'required',
            'nama_lembaga'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(),404);
        }
        $noKtp = $request->get('nik');
        $id_lembaga = $request->get('id_lembaga');
        $email = $request->get('nama_lembaga');
        #PENGECEKAN ID & TOKEN
        $x = explode(' ',$request->header('authorization'));
        $lembaga = User::where('id_lembaga',$id_lembaga)->where('api_token',$x[1])->first();
        if (empty($lembaga)) {
            return response()->json('ID Lembaga not available !',404);
        }
        #ACTIVITY LOG UNTUK LOGIN API
        Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'users' ,'details'=> $email.' trial login to api dengan kode '.$id_lembaga ,'action' => 'Access API'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        #ACTIVITY LOG UNTUK GET DETAIL
        Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'data_balikan' ,'details'=> $email.' view data data_balikan untuk nik '.$noKtp,'updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        $result = DB::table('data_balikan')->where('nik',$noKtp)->where('id_lembaga',$id_lembaga)->get();
        if($result->count() == 0)
        {
            return response()->json("Data Not Found",404);
        }
        return response()->json($result,200);
        
    }
    public function getparam_bylembaga(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'id_lembaga'=>'required',
            'nama_lembaga'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(),404);
        }
        $id_lembaga = $request->get('id_lembaga');
        $email = $request->get('nama_lembaga');
        #PENGECEKAN ID & TOKEN
        $x = explode(' ',$request->header('authorization'));
        if (sizeof($x) > 1) {
            $lembaga = User::where('id_lembaga',$id_lembaga)->where('api_token',$x[1])->first();
            if (empty($lembaga)) {
                return response()->json('ID Lembaga not available !',404);
            }
        } else 
            return response()->json('API Token is invalid !',404);

        #ACTIVITY LOG UNTUK LOGIN API
        Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'users' ,'details'=> $email.' trial login to api dengan kode '.$id_lembaga ,'action' => 'Access API'
           ,'developer'=>0,'language_key'=>0,'public'=>0]);
        #ACTIVITY LOG UNTUK GET DATA PARAMETER
        Activity::log(['contentId'=> 1,'contentType' => 'Access API','description' => 'param_lembaga' ,'details'=> $email.' view data param_lembaga untuk dengan id_lembaga '.$id_lembaga,'updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        $result = DB::table('param_lembaga')->where('id_lembaga',$id_lembaga)->orderBy("indexNo","ASC")->get();
        if($result->count() == 0)
        {
            return response()->json("Data Not Found",404);
        }
        return response()->json($result,200);
        echo "xxx";
        
    }
}
