<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Regulus\ActivityLog\Models\Activity;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class BiodataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
            return abort(404);

         $type = $request->input('type');
        if($request->input('find') && $request->input('nikfind')){
           
            #CARI DATA KTP
            Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Biodata','description' => 'Biodata_WNI' ,'details'=> Auth::user()->email.' cari biodata '.$request->input('nikfind'),'action' => 'Search'
                ,'developer'=>0,'language_key'=>0,'public'=>0]);

            if ($type == "nik") {
                #UNTUK CHECK QUERY
                $headers = ['Accept'=>'application/json','Content-Type'=>'application/json'];
                $filter  = new Client(['verify'=>false]);
                $biodata_ws = $filter->request('POST',Config('app.biodata_wni'),['headers'=>$headers,
                    'json'=>['NIK'=>$request->input('nikfind'),'user_id'=>'2523171201712211data_balik','password'=>'123']]);
                if($biodata_ws->getStatusCode() == 200)
                {
                    Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Biodata','description' => 'Biodata_WNI' ,'details'=> Auth::user()->email.' lihat list biodata '.$request->input('nikfind'),'action' => 'List'
                        ,'developer'=>0,'language_key'=>0,'public'=>0]);

                    $biodata = json_decode($biodata_ws->getBody()->getContents(),false,512,JSON_BIGINT_AS_STRING);
                    if(array_key_exists('RESPON',$biodata->content[0]))
                    {
                        $biodata = "Null"; //$biodata->content[0]->RESPON;
                        Session::flash('flash_message', 'Data Tidak Ditemukan');
                    }
                }
                else
                {
                    $biodata = 'Null';
                }
            } else if ($type == "value") {
                #UNTUK CHECK QUERY
                $result = DB::table("data_balikan")->where("value", $request->input("nikfind"))->select(\DB::raw("nik AS NIK, '' AS NAMA_LGKP "))->get();
                $data = array();
                foreach ($result as $key => $value) {
                    $headers = ['Accept'=>'application/json','Content-Type'=>'application/json'];
                    $filter  = new Client(['verify'=>false]);
                    $biodata_ws = $filter->request('POST',Config('app.biodata_wni'),['headers'=>$headers,
                        'json'=>['NIK'=>$value->nik,'user_id'=>'2523171201712211data_balik','password'=>'123']]);
                    if($biodata_ws->getStatusCode() == 200)
                    {
                        $obj = json_decode($biodata_ws->getBody()->getContents(),false,512,JSON_BIGINT_AS_STRING);
                        if (!isset($obj->content[0]->RESPON)) {
                            $value->nama_lgkp = $obj->content[0]->NAMA_LGKP;
                            array_push($data, $value);
                        }
                    }
                }
                $biodata = (object) ['content' => $data];            
            }
        }else {
            $biodata = "Null"; //DB::table('Biodata_WNI')->get();
        }

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Biodata','description' => 'Biodata_WNI' ,'details'=> Auth::user()->email.' access menu biodata','action' => 'List'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        if($request->input('find') && $request->input('kelurahan')!=''){
            return view('listdata', ['biodata' => $biodata, 'type' => $type]);
        } else {
            return view('listdata', ['biodata' => $biodata, 'type' => $type]);
        }
    }
    public function detail($nik)
    {
        if (Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
            return abort(404);
            
        $headers = ['Accept'=>'application/json','Content-Type'=>'application/json'];
        $filter  = new Client(['verify'=>false]);
        $biodata_ws = $filter->request('POST',Config('app.biodata_wni'),['headers'=>$headers,
            'json'=>['NIK'=>$nik,'user_id'=>'2523171201712211data_balik','password'=>'123']]);
        if($biodata_ws->getStatusCode() == 200){
            $detail = json_decode($biodata_ws->getBody()->getContents(),false,512,JSON_BIGINT_AS_STRING);

        } else
        {
            $detail = 'Null';
        }

        $hasil = DB::table('data_balikan')->join('setup_lembaga','data_balikan.id_lembaga','=','setup_lembaga.id_lembaga')
            ->select('data_balikan.id_lembaga','lembaga_pengguna')->where('nik',$nik)->groupby('data_balikan.id_lembaga','lembaga_pengguna')->get();
        for ($i=0;$i<count($hasil);$i++){
            $lembaga[$i] = $this->getdetail($hasil[$i]->id_lembaga,$nik);
        }

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Biodata','description' => 'Biodata_WNI' ,'details'=> Auth::user()->email.' view detail biodata '.$nik,'action' => 'View'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('detaildatav2', compact('detail','hasil','lembaga'));
    }
    public function getdetail($i,$nik)
    {
        $list = DB::table('data_balikan')->where('id_lembaga',$i)->where('nik',$nik)->get();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Biodata','description' => 'data_balikan' ,'details'=> Auth::user()->email.' list detail biodata dari nik '.$nik,'action' => 'View'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return $list;
    }
    public function getkabupaten()
    {
        $prop_id = Input::get('prop_id');
        $result = DB::table('kode_wilayah_kabupaten')->where('no_prop',$prop_id)->pluck('kab_name','no_kab');
        return Response::make($result);
    }
    public function getkecamatan()
    {
        $prop_id = Input::get('prop_id');
        $kab_id = Input::get('kab_id');
        $result = DB::table('kode_wilayah_kecamatan')->where('no_prop',$prop_id)->where('no_kab',$kab_id)->pluck('kec_name','no_kec');
        return Response::make($result);
    }
    public function getkelurahan()
    {
        $prop_id = Input::get('prop_id');
        $kab_id = Input::get('kab_id');
        $kec_id = Input::get('kec_id');
        $result = DB::table('kode_wilayah_kelurahan')->where('no_prop',$prop_id)->where('no_kab',$kab_id)->where('no_kec',$kec_id)->pluck('kel_name','no_kel');
        return Response::make($result);
    }

}
