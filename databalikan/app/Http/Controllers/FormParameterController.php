<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Auth;
use Regulus\ActivityLog\Models\Activity;


class FormParameterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setParameter()
    {
        if (Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
            return abort(404);
            
        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->orderBy('indexno','ASC')->get();
        $error = 0;
        if($prevParameter->count()>1){
            //message error
            return redirect('editParameter');
        } else {
           $error = 1;
        }
        $namaLembaga = DB::table('setup_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->first();
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' open form parameter ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('parameterform/add',['nama'=>$namaLembaga->lembaga_pengguna, "error" => $error]);
    }

    public function postParameter(Request $request)
    {
        $data = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->orderBy('indexno','DESC')->first();
        if ($data == null)
            $indexno = 0;
        else
            $indexno = $data->indexno+1;

        foreach ($request->import_file as $value) {
            if($value=="")continue;
            $insert = ['id_lembaga' => Auth::user()->id_lembaga, 'parameter' => $value, 'indexno' => $indexno];
            
            if(!empty($insert)){
                $prevParameter = DB::table('param_lembaga')->where("parameter","=",$value)->where('id_lembaga', Auth::user()->id_lembaga)->orderBy('indexno','ASC')->get();
                
                if($prevParameter->count()<=0) {
                    DB::table('param_lembaga')->insert($insert);
                    Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' add new parameter '.json_encode($insert) ,'updated' => 0
                        ,'developer'=>0,'language_key'=>0,'public'=>0]);

                }
            }
            $indexno++;
        }
        return redirect('editParameter');
    }

    public function editParameter()
    {
        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->orderBy('indexno','ASC')->get();
        if($prevParameter->count()<=1){
            return redirect('setParameter');
        } else if (Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
            return abort(404);

        $namaLembaga = DB::table('setup_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->first();
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' form edit parameter untuk '.Auth::user()->id_lembaga ,'updated' => 1
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('parameterform/edit',['prevs' => $prevParameter,'nama'=>$namaLembaga->lembaga_pengguna]);
    }

    public function postEditParameter(Request $request)
    {
        if($request->import_file==null)return back();
        $data = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->orderBy('indexno','DESC')->first();
        if ($data == null)
            $indexno = 0;
        else
            $indexno = $data->indexno+1;
        foreach ($request->import_file as $value) {
            if($value=="")continue;
            $insert[] = ['id_lembaga' => Auth::user()->id_lembaga, 'parameter' => $value, 'indexno' => $indexno];
            $indexno++;
        }
        if(!empty($insert)){
            DB::table('param_lembaga')->insert($insert);
            Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' update parameter untuk '.Auth::user()->id_lembaga ,'updated' => 1
                ,'developer'=>0,'language_key'=>0,'public'=>0]);

        }
        return back();
    }

    public function removeParameter(Request $request)
    {
        DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->where('parameter', $request->input("p"))->delete();
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' remove parameter untuk '.Auth::user()->id_lembaga ,'updated' => 1
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        return back();
    }
}
