<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Excel;
use DB;
use Auth;
use Regulus\ActivityLog\Models\Activity;

class FormFieldController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function form()
    { 
        if (Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
            return abort(404);
            
        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->get();
        if($prevParameter->count()<=1){
            return redirect('setParameter');
        }
        $namaLembaga = DB::table('setup_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' Open form parameter field ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        return view('form/add',['prevs' => $prevParameter,'nama'=>$namaLembaga->lembaga_pengguna, 'message' => '']); 
    }

    public function postForm(Request $request)
    {
        if($request->field==null)return back();
        
        $i =0;
        $noKtp="";
        $date = date_create();
        $time = date_timestamp_get($date);
        foreach ($request->field as $key => $value) {
             if(strtoupper($key) == "NIK") {
                $noKtp = $value;
                break;
             }
        }
        foreach ($request->field as $key => $value) {
            // Fixed by Albert Ricia
            if(strtoupper($key) != "NIK"){
                $insert[] = ['id_lembaga' => Auth::user()->id_lembaga, 'parameter' => $key, 'value' => strtoupper($value), 'nik' => $noKtp,'source'=>1, 'tgl_import' => date('Y-m-d H:i:s')];
            }
            $i++;
        }
        if(!empty($insert)){
            DB::table('data_balikan')->insert($insert);

            Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Param Lembaga','description' => 'data_balikan' ,'details'=> Auth::user()->email.' Add Parameter ','updated' => 0
                ,'developer'=>0,'language_key'=>0,'public'=>0]);
        }
        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->get();
        if($prevParameter->count()<=0){
            return redirect('setParameter');
        }
        $namaLembaga = DB::table('setup_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->first();
        // Session::flash('flash_message', 'Data form berhasil disimpan!');
        // return back();
        return view('form/add',['prevs' => $prevParameter,'nama'=>$namaLembaga->lembaga_pengguna, 'message' => "Data form berhasil disimpan!"]); 
    }
    #UNTUK LEMBAGA PENGGUNA
    public function form_add()
    {
        $prevParameter = DB::table('param_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->get();
        if($prevParameter->count()<=0){
            return redirect('setParameter');
        }
        $namaLembaga = DB::table('setup_lembaga')->where('id_lembaga', Auth::user()->id_lembaga)->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Form Script','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' Open form script input','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('form/form_add',['prevs' => $prevParameter,'nama'=>$namaLembaga->lembaga_pengguna]);
    }
    
}
