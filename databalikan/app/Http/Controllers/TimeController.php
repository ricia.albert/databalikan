<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Auth;
use File;
use Carbon\Carbon;
use App\Jobs\InsertData;
use Regulus\ActivityLog\Models\Activity;
use Illuminate\Support\Facades\Session;


class TimeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if (Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
            return abort(404);
            
        $search = $request->input('s');
        $search = ($search == null) ? "" : $search;
        $sd = $request->input('sd');
        $ed = $request->input('ed');

        $sd = ($sd == null) ? date('Y-m-d') : $sd;
        $ed = ($ed == null) ? date('Y-m-d', strtotime('+1days')) : $ed;

		//ini dikomen karena tgl_import dan last_send berbeda
        //$data = DB::table('data_balikan')->whereBetween('tgl_import', [date('Y-m-d', strtotime($sd)), date('Y-m-d', strtotime($ed))])->join('users', 'data_balikan.id_lembaga', '=', 'users.id_lembaga')->where(\DB::raw("LOWER(email)"),"like","%".strtolower($search)."%")->groupBy('data_balikan.id_lembaga','name','email','last_send')->orderBy("datacount","desc")->select(\DB::raw('name, email, last_send, count(*) as datacount'))->get();
		
		$data = DB::select( DB::raw("select a.id_lembaga,a.name,a.email,b.datacount,c.last_send from users a
		join(select id_lembaga,COUNT(*) AS datacount from data_balikan where TGL_IMPORT between to_date('".$sd."','YYYY-MM-DD') and to_date('".$ed."','YYYY-MM-DD') group by id_lembaga) b on b.id_lembaga=a.id_lembaga
		join(select id_lembaga,max(TGL_IMPORT) as last_send from data_balikan where TGL_IMPORT between to_date('".$sd."','YYYY-MM-DD') and to_date('".$ed."','YYYY-MM-DD') group by id_lembaga) c on c.id_lembaga=a.id_lembaga order by b.datacount desc") );

        $total = 0;
        foreach ($data as $d) {
            $total += $d->datacount;
        }   
        return view('time', ['lembaga' => $data, 'search' => $search, 'total' => $total, 'sd' => $sd, 'ed' => $ed]);
    }
	
	public function ajaxTime(Request $request) {
        if (Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
            return abort(404);
            
        $id_lembaga = $request->input('id_lembaga');
        $sd = $request->input('sd');
        $ed = $request->input('ed');
        $sd = ($sd == null) ? date('Y-m-d') : $sd;
        $ed = ($ed == null) ? date('Y-m-d', strtotime('+1days')) : $ed;
		
		$data=DB::table('data_balikan')->where('id_lembaga',$id_lembaga)->whereBetween('tgl_import', [date('Y-m-d', strtotime($sd)), date('Y-m-d', strtotime($ed))])->distinct()->limit(5)->get();
        return view('ajaxTime', ['lembaga' => $data]);
    }

    public function exportExcel(Request $request) {
        $title = "UPLOAD DATA BALIKAN";
        $dirjen = "DIREKTORAT JENDERAL KEPENDUDUKAN DAN PENCATATAN SIPIL";
        $mendagri = "KEMENTERIAN DALAM NEGERI";

        $headers = "Kode = ".Auth::user()->id_lembaga;
        $note = "NOTE :";
        $note_1 = "1. DILARANG MENAMBAH KOLOM ATAU MENGUBAH HEADER DATA BALIKAN DI ROW KE-12";
        $note_2 = "2. MAKSIMUM ISI 1X UPLOAD SEBANYAK 500.000 ROW";
        $note_3 = "3. DATA AKAN DIAMBIL MULAI ROW KE-13";

        $index =0;
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' melakukan download form excel untuk '.Auth::user()->id_lembaga,'action' => 'Download'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        $search = $request->input('s');
        $search = ($search == null) ? "" : $search;
        $sd = $request->input('sd');
        $ed = $request->input('ed');

        $sd = ($sd == null) ? date('Y-m-d') : $sd;
        $ed = ($ed == null) ? date('Y-m-d', strtotime('+1days')) : $ed;

        $data = DB::select( DB::raw("select a.id_lembaga,a.name,a.email,b.datacount,c.last_send from users a
		join(select id_lembaga,COUNT(*) AS datacount from data_balikan where TGL_IMPORT between to_date('".$sd."','YYYY-MM-DD') and to_date('".$ed."','YYYY-MM-DD') group by id_lembaga) b on b.id_lembaga=a.id_lembaga
		join(select id_lembaga,max(TGL_IMPORT) as last_send from data_balikan where TGL_IMPORT between to_date('".$sd."','YYYY-MM-DD') and to_date('".$ed."','YYYY-MM-DD') group by id_lembaga) c on c.id_lembaga=a.id_lembaga order by b.datacount desc") );

        return Excel::create('format_upload', function($excel) use ($data, $sd, $ed) {
            $excel->sheet('Laporan Lembaga', function($sheet) use ($data, $sd, $ed)
            {
                /*$sheet->row(1, array($title));
                $sheet->row(2,array($dirjen));
                $sheet->row(3,array($mendagri));
                $sheet->row(5,array($headers));
                $sheet->row(7,array($note));
                $sheet->row(8,array($note_1));
                $sheet->row(9,array($note_2));
                $sheet->row(10,array($note_3));*/
                $val = array("No.", "ID Lembaga", "Lembaga Pengguna", "Jumlah Data", "Terakhir Kirim");
                $no = 1;
                $row = 3;
                $sheet->row($row, $val);
                foreach ($data as $key => $value) {
                    $val = array();
                    $val[] = $no;
                    $val[] = $value->name;
                    $val[] = $value->email; 
                    $val[] = $value->datacount;
                    $val[] = date('d M Y H:i',strtotime($value->last_send));
                    $no++;
                    $row++;
                    $sheet->row($row, $val);
                }
                
                $sheet->setCellValue('A1', 'Dari tanggal '.date('d M Y', strtotime($sd)).' s/d '.date('d M Y', strtotime($ed)));

                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->cells('A1:E3', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));

                });

                $sheet->mergeCells('A1:E1');
            });
        })->download("xlsx");
    }
}
