<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Auth;
use File;
use Carbon\Carbon;
use App\Jobs\InsertData;
use Regulus\ActivityLog\Models\Activity;
use Illuminate\Support\Facades\Session;


class GraphController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if (Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
            return abort(404);
            
        $search = $request->input('s');
        $search = ($search == null) ? "" : $search;
        $sd = $request->input('sd');
        $ed = $request->input('ed');
        $id_lembaga = $request->input("lmb");
        $type = $request->input("type");

        $sd = ($sd == null) ? date('Y-m-d', strtotime('-31days')) : $sd;
        $ed = ($ed == null) ? date('Y-m-d', strtotime('+1days')) : $ed;
        $type = ($type == null) ? "D" : $type;


        $lembaga = DB::table('setup_lembaga')->where("jenis_lembaga","!=","ADMINISTRATOR")->orderBy("lembaga_pengguna", "asc")->get();

        if ($type == "D") {
            $data = DB::select("select to_char(COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')),'YYYY-mm-dd') AS dates, COUNT(1) AS jml from data_balikan
                                where id_lembaga = '".$id_lembaga."' and COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')) between to_date('".date('Y-m-d',strtotime($sd))."','YYYY-MM-DD') AND to_date('".date('Y-m-d',strtotime($ed))."','YYYY-MM-DD')
                                group by to_char(COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')),'YYYY-mm-dd')
                                ORDER BY dates ASC");
        } else if ($type == "M") {
            $data = DB::select("select to_char(COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')),'YYYY-mm') AS dates, COUNT(1) AS jml from data_balikan
                                where id_lembaga = '".$id_lembaga."' and COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')) between to_date('".date('Y-m-d',strtotime($sd))."','YYYY-MM-DD') AND to_date('".date('Y-m-d',strtotime($ed))."','YYYY-MM-DD')
                                group by to_char(COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')),'YYYY-mm')
                                ORDER BY dates ASC"); 
        } else if ($type == "Y") {
            $data = DB::select("select to_char(COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')),'YYYY') AS dates, COUNT(1) AS jml from data_balikan
                                where id_lembaga = '".$id_lembaga."' and COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')) between to_date('".date('Y-m-d',strtotime($sd))."','YYYY-MM-DD') AND to_date('".date('Y-m-d',strtotime($ed))."','YYYY-MM-DD')
                                group by to_char(COALESCE(tgl_import,to_date('2018-12-31','YYYY-mm-dd')),'YYYY')
                                ORDER BY dates ASC"); 
        }
        // $data = DB::table('data_balikan')->whereBetween('tgl_import', [date('Y-m-d', strtotime($sd)), date('Y-m-d', strtotime($ed))])->join('users', 'data_balikan.id_lembaga', '=', 'users.id_lembaga')->where("id_lembaga","like","%".strtolower($search)."%")->groupBy('data_balikan.id_lembaga','name','email','last_send')->orderBy("datacount","desc")->select(\DB::raw('name, email, last_send, count(*) as datacount'))->get();
        $time = array();
        $value = array();
        $total = 0;
        foreach ($data as $d) {
            array_push($time, $d->dates);
            array_push($value, $d->jml);
            $total += $d->jml;
        }

        // foreach ($data as $d) {
        //     $total += $d->datacount;
        // }   
        return view('graph', ['lembaga' => $lembaga, 'data' => $data, 'value' => json_encode($value, JSON_NUMERIC_CHECK),'time' => json_encode($time), 'search' => $id_lembaga, 'total' => $total, 'sd' => $sd, 'ed' => $ed, 'type' => $type]);
    }

    public function exportExcel(Request $request) {

        $id_lembaga = $request->input('lmb');
        $sd = $request->input('sd');
        $ed = $request->input('ed');
        $type = $request->input('type');
        $typeNm = "";
        if ($type == "D") 
            $typeNm = "Harian";
        else if ($type == "M")
            $typeNm = "Bulanan";
        else if ($type == "Y")
            $typeNm = "Tahunan";

         $lembaga = DB::table('setup_lembaga')->where("id_lembaga","=",$id_lembaga)->first();

        $title = "LAPORAN STATISTIK ".strtoupper($typeNm)." ".$lembaga->lembaga_pengguna;
        $dirjen = "DIREKTORAT JENDERAL KEPENDUDUKAN DAN PENCATATAN SIPIL";
        $mendagri = "KEMENTERIAN DALAM NEGERI";

        $headers = "Kode = ".$id_lembaga;
        $note = "NOTE :";
        $note_1 = "1. DILARANG MENAMBAH KOLOM ATAU MENGUBAH HEADER DATA BALIKAN DI ROW KE-12";
        $note_2 = "2. MAKSIMUM ISI 1X UPLOAD SEBANYAK 500.000 ROW";
        $note_3 = "3. DATA AKAN DIAMBIL MULAI ROW KE-13";

        $index =0;
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Upload','description' => 'param_lembaga' ,'details'=> Auth::user()->email.' melakukan download laporan statistik untuk '.$lembaga->lembaga_pengguna,'action' => 'Download'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

       

        $sd = ($sd == null) ? date('Y-m-d') : $sd;
        $ed = ($ed == null) ? date('Y-m-d', strtotime('+1days')) : $ed;

        if ($type == "D") {
            $data = DB::select("select to_char(tgl_import,'YYYY-mm-dd') AS dates, COUNT(1) AS jml from data_balikan
                                where id_lembaga = '".$id_lembaga."' and tgl_import between to_date('".date('Y-m-d',strtotime($sd))."','YYYY-MM-DD') AND to_date('".date('Y-m-d',strtotime($ed))."','YYYY-MM-DD')
                                group by to_char(tgl_import,'YYYY-mm-dd')
                                ORDER BY dates ASC");
        } else if ($type == "M") {
            $data = DB::select("select to_char(tgl_import,'YYYY-mm') AS dates, COUNT(1) AS jml from data_balikan
                                where id_lembaga = '".$id_lembaga."' and tgl_import between to_date('".date('Y-m-d',strtotime($sd))."','YYYY-MM-DD') AND to_date('".date('Y-m-d',strtotime($ed))."','YYYY-MM-DD')
                                group by to_char(tgl_import,'YYYY-mm')
                                ORDER BY dates ASC"); 
        } else if ($type == "Y") {
            $data = DB::select("select to_char(tgl_import,'YYYY') AS dates, COUNT(1) AS jml from data_balikan
                                where id_lembaga = '".$id_lembaga."' and tgl_import between to_date('".date('Y-m-d',strtotime($sd))."','YYYY-MM-DD') AND to_date('".date('Y-m-d',strtotime($ed))."','YYYY-MM-DD')
                                group by to_char(tgl_import,'YYYY')
                                ORDER BY dates ASC"); 
        }

        return Excel::create('format_upload', function($excel) use ($data, $sd, $ed, $title) {
            $excel->sheet('Laporan Lembaga', function($sheet) use ($data, $sd, $ed, $title)
            {
                /*$sheet->row(1, array($title));
                $sheet->row(2,array($dirjen));
                $sheet->row(3,array($mendagri));
                $sheet->row(5,array($headers));
                $sheet->row(7,array($note));
                $sheet->row(8,array($note_1));
                $sheet->row(9,array($note_2));
                $sheet->row(10,array($note_3));*/
                $val = array("No.", "Tanggal Input", "Jumlah Data");
                $no = 1;
                $row = 4;
                $sheet->row($row, $val);
                foreach ($data as $key => $value) {
                    $val = array();
                    $val[] = $no;
                    $val[] = $value->dates;
                    $val[] = $value->jml; 
                    $no++;
                    $row++;
                    $sheet->row($row, $val);
                }
                
                $sheet->setCellValue('A1', $title);
                $sheet->setCellValue('A2', 'Dari tanggal '.date('d M Y', strtotime($sd)).' s/d '.date('d M Y', strtotime($ed)));

                $sheet->setBorder('A4:C'.$row, 'thin');
                $sheet->cells('A1:C4', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));

                });

                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
            });
        })->download("xlsx");
    }
}
