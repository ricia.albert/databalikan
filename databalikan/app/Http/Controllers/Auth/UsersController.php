<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Auth;
use DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $result = DB::table("users")->orderBy("id_lembaga", "ASC")->get();

        return view('users.list',compact('result'));
    }
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name' => 'required|string|max:100|unique:users',
        ]);
    }
    protected function tambah(array $data)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Add Users','description' => 'users' ,'details'=> Auth::user()->email.' tambah data users '.json_encode($data) ,'updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'id_lembaga'=>$data['id_lembaga'],
            'password' => bcrypt($data['password']),
        ]);
    }
    public function create()
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Add Users','description' => 'users' ,'details'=> Auth::user()->email.' open form tambah data users ','updated' => 0
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('users.add');
    }
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->tambah($request->all());

        return redirect('users');
    }

    public function ubah($id_lembaga)
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $detail = User::where('id',$id_lembaga)->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Reset Token','description' => 'users' ,'details'=> Auth::user()->email.' view data token lembaga'.$id_lembaga,'action' => 'View'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('users.edit',compact('detail'));
    }

    public function modify(Request $request)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Edit Users','description' => 'users' ,'details'=> Auth::user()->email.' ubah data users'.$request->input('email'),'updated' => 1
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        User::where('id',$request->input('id'))->update(['name' => $request->input('name'),
                                                         'email' => $request->input('email'),
                                                         'id_lembaga'=>$request->input('id_lembaga')]);

        Session::flash('flash_message', 'Data users berhasil diubah!');
        return redirect('users');
    }

    public function edit($id_lembaga)
    {
        if (Auth::user()->id_lembaga != "ADMIN")
            return abort(404);

        $detail = User::where('id_lembaga',$id_lembaga)->first();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Reset Token','description' => 'users' ,'details'=> Auth::user()->email.' view data token lembaga'.$id_lembaga,'action' => 'View'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        return view('auth.token',compact('detail'));
    }

    public function update(Request $request)
    {
        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Reset Token','description' => 'users' ,'details'=> Auth::user()->email.' reset data token lembaga'.$request->input('id_lembaga'),'updated' => 1
            ,'developer'=>0,'language_key'=>0,'public'=>0]);

        User::where('id_lembaga',$request->input('id_lembaga'))->update(['api_token'=>$request->input('api_token')]);

        Session::flash('flash_message', 'Reset token berhasil diubah!');
        return redirect('users');
    }
    public function destroy($user_id)
    {
        $users = User::find($user_id);
        #hapus data lembaga
        $lembaga = DB::table('setup_lembaga')->where('id_lembaga',$users->id_lembaga)->delete();

        #hapus data users
        $users->delete();

        Session::flash('flash_message', 'User Lembaga Pengguna berhasil dihapus');
        return redirect('users');
    }
    public function generate()
    {
        $result = str_random(100);

        return Response::json($result);
    }
}
