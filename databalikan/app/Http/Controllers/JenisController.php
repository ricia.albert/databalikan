<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Regulus\ActivityLog\Models\Activity;
use Auth;
use Validator;
use App\User;

class JenisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $result = DB::table('setup_lembaga')->get();

        Activity::log(['contentId'=> Auth::user()->id,'contentType' => 'Lembaga','description' => 'setup_lembaga' ,'details'=> Auth::user()->email.' view list lembaga','action' => 'List'
            ,'developer'=>0,'language_key'=>0,'public'=>0]);
        return view('jenis.list',compact('result'));
    }
}
