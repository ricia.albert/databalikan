<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Regulus\ActivityLog\Models\Activity;

class InsertData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileinput, $id_lembaga, $user_id, $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileinput,$id_lembaga,$user_id,$email)
    {
        $this->fileinput = $fileinput;
        $this->id_lembaga = $id_lembaga;
        $this->user_id = $user_id;
        $this->email = $email;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = Excel::load(storage_path().'/uploads/'.$this->fileinput, function($reader) {
            $reader->noHeading();
            $reader->limitRows(500000);
        })->get();

        ini_set('max_execution_time', 72000);
        if(!empty($data) && $data->count()) {
            $insert = array();
            foreach ($data as $i => $value){
                $noKtp="";
                $date = date_create();
                $time = date_timestamp_get($date);
                if($i == 0){
                    $arrayKeys = $value;
                }
                if($i > 0){
                    $insert = array();
                    foreach (array_chunk($value->toArray(),15000) as $item) {
                        $j=0;
                        foreach ($item as $arrayKey=>$arrayValue) {
                            if($j==0){
                                $noKtp = $arrayValue;
                            }
                            if($arrayValue!=null) {
                                $review = DB::table('data_balikan')->where('parameter',$arrayKeys[$arrayKey])->where('value',$arrayValue)->where('nik',$noKtp)->first();
                                if(!$review){
                                    $insert[] = ['id_lembaga' => $this->id_lembaga, 'parameter' => $arrayKeys[$arrayKey], 'value' => $arrayValue, 'nik' => $noKtp, 'time'=> $time,'source'=>2];
                                    $j++;
                                }
                            }
                        }
                    }
                    DB::table('data_balikan')->insert($insert);
                    unset($insert);
                }
            }

            $totalRows = sizeof($data);
            Activity::log(['contentId' => $this->user_id, 'contentType' => 'Upload', 'description' => 'import_file', 'details' => $this->email . ' melakukan import file untuk ' . $this->id_lembaga . ' , jumlah baris : ' . ($totalRows - 1), 'updated' => 0
                , 'developer' => 0, 'language_key' => 0, 'public' => 0]);
        }
    }
}
