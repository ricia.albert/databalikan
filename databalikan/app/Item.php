<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Item extends Model
{
   public $fillable = ['id_lembaga','parameter','value','no_ktp','time','source'];
}
