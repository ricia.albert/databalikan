<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Provinsi extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_prop', 'nama_prop', 'flagsink','last_upd_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
