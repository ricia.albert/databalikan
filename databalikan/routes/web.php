<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/text',function(){
    phpinfo();
});
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
#API
Route::get('api/getparam_bylembaga', 'Admin\APIController@getparam_bylembaga');
Route::post('api/store', 'Admin\APIController@store');
Route::group(['middleware'=>['web','auth']], function () {
    Route::get('/upload', function() {
        return view('upload');
    });

    Route::get('/listdata', 'Admin\BiodataController@index');
    Route::post('/listdata', 'Admin\BiodataController@index');
    Route::get('/detaildata/{nik}', 'Admin\BiodataController@detail');

    Route::get('/listlembaga','Admin\ParamController@index');
    Route::get('/listlembaga/create','Admin\ParamController@create');
    Route::post('/listlembaga/create','Admin\ParamController@store');
    Route::get('/listlembaga/remove','Admin\ParamController@delete');
    Route::get('/listlembaga/edit','Admin\ParamController@edit');
    Route::post('/listlembaga/edit','Admin\ParamController@modify');

    Route::get('/listslider','SliderController@index');
    Route::get('/listslider/create','SliderController@create');
    Route::post('/listslider/create','SliderController@tambah');
    Route::get('/listslider/remove','SliderController@delete');
    Route::get('/listslider/edit','SliderController@edit');
    Route::post('/listslider/edit','SliderController@update');

    #UNTUK GET KAB, KEC, KEL
    Route::get('/admin/kab/', 'Admin\BiodataController@getkabupaten');
    Route::get('/admin/kec/', 'Admin\BiodataController@getkecamatan');
    Route::get('/admin/kel/', 'Admin\BiodataController@getkelurahan');

    #TRIAL 
    Route::get('/upload', 'UploadController@index');
    Route::post('/uploadPost', 'UploadController@uploadFile');
    Route::get('/generate','Auth\UsersController@generate');

    #RESET TOKEN / ACTIVITY / PASSWORD
    Route::get('/admin/activity', 'Admin\ActivityController@index');
/*    Route::get('/admin/token/{id_lembaga}','Auth\UsersController@edit');
    Route::post('/admin/token','Auth\UsersController@update');*/
    Route::get('/admin/getactivity','Admin\ActivityController@getactivity');
    Route::get('/admin/reset/{id_lembaga}','Admin\ParamController@reset');
    Route::post('/admin/reset','Admin\ParamController@update');


    Route::get('/setParameter', 'FormParameterController@setParameter')->name('format');
    Route::post('/postParameter', 'FormParameterController@postParameter')->name('postformat');
    Route::get('/editParameter', 'FormParameterController@editParameter')->name('editformat');
    Route::post('/postEditParameter', 'FormParameterController@postEditParameter')->name('postEditformat');
    Route::get('/removeParameter', 'FormParameterController@removeParameter')->name('removeformat');

    Route::get('/form', 'FormFieldController@form')->name('form');
    Route::post('/formPost', 'FormFieldController@postForm')->name('formPost');
    Route::get('/formscript', 'FormFieldController@form_add')->name('formscript');
    Route::post('/formupload', 'FormFieldController@form_script')->name('formupload');

    Route::get('downloadExcel/{type}', 'UploadController@downloadExcel');
    Route::get('downloadCSV/{type}', 'UploadController@downloadCSV');
    Route::match(['get','post'],'listbalikan','ActivityController@listbalikan');

    #USERS
    Route::get('/users/','Auth\UsersController@index');
    #CREATE_USERS
    Route::get('/users/create','Auth\UsersController@create');
    Route::post('/users/create','Auth\UsersController@store');
    Route::get('/users/ubah/{id_lembaga}','Auth\UsersController@ubah');
    Route::post('/users/modify','Auth\UsersController@modify');
    #TOKEN USERS
    Route::get('/users/token/{id_lembaga}','Auth\UsersController@edit');
    Route::post('/users/token','Auth\UsersController@update');
    Route::delete('/users/{user_id}',array('uses' => 'Auth\UsersController@destroy', 'as' => 'users.destroy'));
    Route::get('/users/reset/{id_lembaga}','Admin\ParamController@reset');
    Route::post('/users/reset','Admin\ParamController@update');

	Route::get('/time/ajaxTime','TimeController@ajaxTime');
	
});