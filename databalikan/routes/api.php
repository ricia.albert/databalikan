<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::fallback(function(){
    return response()->json(401);
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>['auth:api']],function(){
    Route::get('getdetail','Admin\APIController@getdetail');
    Route::post('store','Admin\APIController@store');
    Route::post('nik_store','Admin\APIController@nik_store');
    Route::get('getparam_bylembaga','Admin\APIController@getparam_bylembaga');
});