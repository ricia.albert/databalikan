<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'tns'            => env('DB_TNS', ''),
        'host'           => env('DB_HOST', 'node01id-under.cloud.revoluz.io'),
        'port'           => env('DB_PORT', '49484'),
        'database'       => env('DB_DATABASE', 'xe'),
        'username'       => env('DB_USERNAME', 'db_data_balikan'),
        'password'       => env('DB_PASSWORD', 'db_data_balikan'),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'server_version' => env('DB_SERVER_VERSION', '11g'),
    ],
];
