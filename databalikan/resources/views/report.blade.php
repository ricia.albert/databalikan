@extends('layouts.template')
@section('content')
<section id="section1">
<div class="sixteen columns text-center" style="margin-top: 100px"><h3>Report Data</h3></div>
</section>
<section id="section2" class="overview container">
<form action="{{ url('/report') }}" method="GET">
	<div class="pull-left" style="padding:12px 12px 0px 12px">
		Total : <strong>{{number_format($total,0,",",".")}} data</strong>
	</div>
	<div class="pull-right searchbox">
		<table>
			<tr>
				<td width="380px"><input type="text" name="s" class="form-control" value="{{ $search }}" placeholder="Cari disini.." /></td>
				<td width="80px"><button type="submit" class="btn btn-success"><span class="fa fa-search"></span> &nbsp;Cari</button></td>
			</tr>
		</table>
	</div>
</form>
<?php $no = 1; ?>
<table class="table">
	<thead>
		<tr>
			<th width="30px" class="text-center"><strong>No.</strong></th>
			<th width="12%" class="text-left"><strong>ID Lembaga</strong></th>
			<th width="30%" class="text-left"><strong>Lembaga Pengguna</strong></th>
			<th width="18%" class="text-left"><strong>Jenis Lembaga</strong></th>
			<th width="15%" class="text-right"><strong>Jumlah Data</strong></th>
			<th width="110px" class="text-center"><strong>Terakhir Kirim</strong></th>
		</tr>
	</thead>
	<tbody>
	@foreach($lembaga as $key => $data)
		<tr>
			<td>{{$no}}</td>
			<td class="text-left"><strong>{{ $data->name }}</strong></td>
			<td class="text-left"><strong>{{ $data->email }}</strong></td>
			<td class="text-left"><strong>{{ $data->jenis_lembaga }}</strong></td>
			<td class="text-right"><strong>{{ number_format($data->datacount,0,",",".") }}</strong></td>
			<td class="text-center">{{ \Carbon\Carbon::parse($data->last_send)->format('d M Y H:i') }}</strong></center></td>
		</tr>
	<?php $no++ ?>
	@endforeach
	</tbody>
</table>
</section>
@endsection
