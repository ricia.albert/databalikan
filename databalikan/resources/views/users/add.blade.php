@extends('layouts.template')
@section('title', 'Add User Login')
@section('content')
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">&nbsp;</h4> </div>
    	<!-- /.col-lg-12 -->
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">&nbsp;</h3>
                        <h3 class="box-title">@yield('title')</h3>
                    	<form class="form-horizontal" method="POST" action="{{ url('users/create') }}">
                        {{ csrf_field() }}
					<div class="box-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">User Login</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Masukkan login user">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Nama Pengguna</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required  placeholder="Masukkan nama user">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('id_lembaga') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Hak Akses</label>

                            <div class="col-md-6">
								<select name="id_lembaga" id="id_lembaga" class="form-control">
                                	<option value="">Pilih Hak Akses</option>
                                    <option value="ADMIN">ADMIN</option>
                                    <option value="SUBADM">SUB ADMIN</option>
                                    <option value="USERS">USERS</option>
                                </select>
                                @if ($errors->has('id_lembaga'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_lembaga') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="Tambah">
                            </div>
                        </div>
                    </form>
                </div>
          </div>
          </section>
@stop
