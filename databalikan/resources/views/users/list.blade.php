@extends('layouts.template')
@section('title', 'List User Account')
@section('content')
    <section id="section2" class="container">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                	<h3 class="box-title">&nbsp;</h3>
                	<h3 class="box-title"><a href="{{ url('/users/create') }}" class="btn btn-info btn-primary btn-sm"><i class="fa fa-binoculars"></i> Tambah User</a></h3>
                    <h3 class="box-title">List User Account</h3>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif                    
                	<div class="table-responsive">
                        <br>
                        <table id="myTable" class="table table-striped nowrap">
                            <thead>
                            <tr>
                                <th width="30px">No</th>
                                <th>User</th>
                                <th width="40%">Nama User</th>
                                <th width="15%">ID Lembaga</th>
                                <th width="260px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result as $key=>$value)
                                <tr>
                                    <td style="width: 30px">{!! $key+1 !!}</td>
                                    <td width="12%">{!! $value->name !!}</td>
                                    <td width="40%">{!! $value->email !!}</td>
                                    <td width="15%">{!! $value->id_lembaga !!}</td>
                                    <td width="200px">
                                    <a href="{{ url('users/ubah/'.$value->id) }}" class="btn btn-info btn-warning btn-sm">Edit</a>
                                    <a href="{{ url('users/reset/'.$value->id) }}" class="btn btn-info btn-primary btn-sm">Password</a>
                                    <a href="{{ url('users/token/'.$value->id_lembaga ) }}" class="btn btn-info btn-primary btn-sm">Token WS</a>
                                    {!! Form::open(['method' => 'DELETE', 'class' => 'inline' ,'route'=>['users.destroy',$value->id]]) !!} 
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin menghapus Account?')">Hapus</button>
                                    {{ Form::close() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@stop
@section('custom-css-js')
    <script>
        (function($) {
            $('#myTable').DataTable({
                "pageLength": 50
            });
        }) (jQuery);
    </script>
@stop