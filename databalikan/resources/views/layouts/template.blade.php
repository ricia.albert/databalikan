<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>Aplikasi Data Balikan v.1.0.</title>
<meta name="description" content="Aplikasi Data Balikan v.1.0">
<meta name="keywords" content="Aplikasi, app, ios, android, showcase, stunning">
<meta name="author" content="Aether Themes">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="{{ url('/plugins/images/admin-logo.png') }}">
<link rel="apple-touch-icon" href="{{ url('/plugins/images/admin-logo.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ url('/plugins/images/admin-logo.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ url('/plugins/images/admin-logo.png') }}">
<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->


<script src="{{ url('/js/jquery.js') }}"></script>
<script src="{{ url('/js/bootstrap.min.js') }}"></script>
<link href="{{ url('/css/icons/material-design-iconic-font/css/materialdesignicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('/css/font/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ url('/css/simple-sidebar.css') }}" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100' rel='stylesheet' type='text/css'>
<link href="{{ url('/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ url('/css/buttons.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ url('/jqueryui/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ url('/jqueryui/jquery-ui.structure.min.css') }}" rel="stylesheet">
<link href="{{ url('/jqueryui/jquery-ui.theme.min.css') }}" rel="stylesheet">
<link href="{{ url('/css/buttons.dataTables.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ url('/css/reset.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/css/grid.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}">
<link href="{{ url('/plugins/slider/js-image-slider.css') }}" rel="stylesheet" type="text/css" />
<!-- <link href="{{ url('/plugins/slider/generic.css') }}" rel="stylesheet" type="text/css" /> -->
<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="stylesheets/ie.css" />
	<![endif]-->
</head>
<body>

<div id="mobile-nav">
<div class="container clearfix">
<div>

<div class="navigationButton sixteen columns clearfix">
<img src="{{ url('/images/mobile-nav.png') }}" alt="Navigation" width="29" height="17" />
</div>

<div class="navigationContent sixteen columns clearfix">
@if (!Auth::guest())
<ul>
<li><a href="{{ url('/') }}">Home</a></li>
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM" || Auth::user()->id_lembaga == "USERS")
<li><a href="{{ url('listdata') }}">Data Penduduk</a></li>
@endif
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
<li class="dropdown">
    <a data-toggle="dropdown" href="#">Lembaga <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ url('listlembaga') }}">Daftar Lembaga</a></li>
        <!-- <li><a href="{{ url('report') }}""><i class="fa fa-power-off"></i> Report Lembaga</a></li> -->
    </ul>
</li>
@if(Auth::user()->id == "1")
<!-- <li><a href="{{ url('users') }}">User Akses</a></li> -->
@endif
<li><a href="{{ url('/admin/activity') }}">Log System</a></li>
@elseif(Auth::user()->id_lembaga != "USERS")
<li> <a href="{{ url('setParameter') }}" >Setting</a></li>
<li> <a href="{{ url('upload') }}" >Upload Data</a> </li>
<li> <a href="{{ url('form') }}" >Form Online</a></li>
<li> <a href="http://172.16.160.31/lembaga/?i={{ Auth::user()->id_lembaga }}" target="_blank" >Script Popup</a></li>
@endif
<li class="dropdown">
	<a data-toggle="dropdown" href="#">{{ Auth::user()->name }} <span class="caret"></span></a>
    <ul class="dropdown-menu">
    @if(Auth::user()->id_lembaga == "ADMIN")
    <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
    @endif
    <li><a href="{{ url('admin/reset/'.Auth::user()->id) }}">Reset Password</a></li>
    <li><a href="{{ route('logout') }}" onClick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form></li>
    </ul>
</li>
</ul>
@endif
</div>
</div>
</div>
</div>

<div id="wrapper">
    <!-- Side Bar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#">
                    <span class="fa fa-navicon"></span> &nbsp;&nbsp; Navigation
                </a>
            </li>
            <li>
                <a href="{{ url('/') }}"><span class="fa fa-home"></span> &nbsp;&nbsp; Home</a>
            </li>
            @if(Auth::user()->id_lembaga == "ADMIN")
                <li>
                    <a title="Dashboard" href="{{ url('dashboard') }}"><span class="fa fa-dashboard"></span> &nbsp;&nbsp; Dashboard</a>
                </li>
                <li>
                    <a title="Data Penduduk" href="{{ url('listdata') }}"><span class="fa fa-users"></span> &nbsp;&nbsp; Data Penduduk</a>
                </li>
                 <li>
                    <a title="List Lembaga" href="{{ url('listlembaga') }}"><span class="fa fa-fort-awesome"></span> &nbsp;&nbsp; List Lembaga</a>
                </li>
                <li>
                    <a title="Log System" href="{{ url('/admin/activity') }}"><span class="fa fa-file"></span> &nbsp;&nbsp; Log System</a>
                </li>
                <li>
                    <a title="Grafik Statistik" href="{{ url('/graph') }}"><span class="fa fa-line-chart"></span> &nbsp;&nbsp; Grafik Statistik</a>
                </li>
                <li>
                    <a title="Slider" href="{{ url('listslider') }}"><span class="fa fa-film"></span> &nbsp;&nbsp; Slider</a>
                </li>
                <li>
                    <a title="User" href="{{ url('users') }}"><span class="fa fa-user-secret"></span> &nbsp;&nbsp; User</a>
                </li>
                <li>
                    <a title="Laporan" href="{{ url('time') }}"><span class="fa fa-book"></span> &nbsp;&nbsp; Laporan</a>
                </li>
            @elseif(Auth::user()->id_lembaga == "SUBADM")
                <li>
                    <a title="Dashboard" href="{{ url('dashboard') }}"><span class="fa fa-dashboard"></span> &nbsp;&nbsp; Dashboard</a>
                </li>
                <li>
                    <a title="Data Penduduk" href="{{ url('listdata') }}"><span class="fa fa-users"></span> &nbsp;&nbsp; Data Penduduk</a>
                </li>
                <li>
                    <a title="Log System" href="{{ url('/admin/activity') }}"><span class="fa fa-file"></span> &nbsp;&nbsp; Log System</a>
                </li>
                <li>
                    <a title="Grafik Statistik" href="{{ url('/graph') }}"><span class="fa fa-line-chart"></span> &nbsp;&nbsp; Grafik Statistik</a>
                </li>
                <li>
                    <a title="Laporan" href="{{ url('time') }}"><span class="fa fa-book"></span> &nbsp;&nbsp; Laporan</a>
                </li>
            @elseif(Auth::user()->id_lembaga != "USERS")
                <li>
                    <a title="Setting Parameter" href="{{ url('setParameter') }}"><span class="fa fa-gear"></span> &nbsp;&nbsp; Setting Parameter</a>
                </li>
                <li>
                    <a title="Download Template Excel" href="{{ URL::to('downloadExcel/xls') }}"><span class="fa fa-file-excel-o"></span> &nbsp;&nbsp; Download Template Excel</a>
                </li>
                 <li>
                    <a title="Download Template CSV" href="{{ URL::to('downloadCSV/csv') }}"><span class="fa fa-file-archive-o"></span> &nbsp;&nbsp; Download Template CSV</a>
                </li>
                <li>
                    <a title="Setting Web Services" href="http://172.16.160.31/lembaga/admin/parameter?i={{ Auth::user()->id_lembaga }}"><span class="fa fa-globe"></span> &nbsp;&nbsp; Setting Web Services</a>
                </li>
                <li>
                    <a title="Upload Data" href="{{ url('upload') }}"><span class="fa fa-upload"></span> &nbsp;&nbsp; Upload Data</a>
                </li>
                <li>
                    <a title="Form Online" href="{{ url('form') }}"><span class="fa fa-file"></span> &nbsp;&nbsp; Form Online</a>
                </li>
            @endif
        </ul>
    </div>
<header class="clearfix" style="background: #2196F3">
<div class="container">
<div id="logo" class="three columns">
    <!-- <img src="{{ url('/images/mobile-nav.png') }}" alt="Navigation" width="29" height="17" /> -->
    <!-- &nbsp; -->
    <img src="{{ url('/plugins/images/admin-logo.png') }}" alt="">  
    
    KEMENDAGRI
</div>
<nav id="navigation" class="thirteen columns pull-right">
@if (!Auth::guest())
<ul class="clearfix">
<!--<li><a class="current" style="color:#fff;" href="{{ url('/') }}">Home</a></li>
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
<li><a style="color:#fff;" href="{{ url('dashboard') }}">Dashboard</a></li>
@endif
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM" || Auth::user()->id_lembaga == "USERS")
<li><a style="color:#fff;" href="{{ url('listdata') }}">Data Penduduk</a></li>
@endif
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
 <li><a style="color:#fff;" href="{{ url('listlembaga') }}">Data Lembaga</a></li>
<li class="dropdown">
    <a style="color:#fff;" data-toggle="dropdown" href="#">Lembaga <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ url('listlembaga') }}">List Lembaga</a></li>
        <!-- <li><a href="{{ url('report') }}""><i class="fa fa-power-off"></i> Report Lembaga</a></li>
        <li><a style="color:#000;" href="{{ url('/admin/activity') }}">Log System</a></li>
        <!-- <li><a href="{{ url('time') }}""><i class="fa fa-power-off"></i> Report Berdasarkan Waktu</a></li>
    </ul>
</li>
<li><a style="color:#fff;" href="{{ url('users') }}">User</a></li>
<li>
    <a style="color:#fff;" href="{{ url('time') }}">Laporan</a>
    <!-- <a style="color:#fff;" data-toggle="dropdown" href="#">Report <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ url('report') }}">Report Lembaga</a></li>
        <li><a href="{{ url('time') }}">Report Berdasarkan Waktu</a></li>
    </ul> 
</li>
    @if(Auth::user()->id == "1" || Auth::user()->id == "704" )
     <li><a style="color:#fff;" href="{{ url('users') }}">User Akses</a></li>
    @endif

@elseif(Auth::user()->id_lembaga != "USERS")
<li class="dropdown"> 
    <a style="color:#fff;" data-toggle="dropdown" href="#">Setting <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a style="color:#000;" href="{{ url('setParameter') }}" >Setting Parameter</a></li>
        <li><a style="color:#000;" href="{{ URL::to('downloadExcel/xls') }}" >Download Template Excel</a></li>
        <li><a style="color:#000;" href="{{ URL::to('downloadCSV/csv') }}" >Download Template CSV</a></li>
        <li><a style="color:#000;" href="http://172.16.160.31/lembaga/admin/parameter?i={{ Auth::user()->id_lembaga }}" target="_blank" >Setting Web Service</a></li> 
    </ul>
</li>
<li class="dropdown"> 
    <a style="color:#fff;" data-toggle="dropdown" href="#">Upload <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a style="color:#000;" href="{{ url('upload') }}" >Upload Data</a></li>
        <li><a style="color:#000;" href="{{ url('form') }}" >Form Online</a></li>
    </ul>
</li>
@endif
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
 <li> <a style="color:#fff;" href="http://172.16.160.31/lembaga" target="_blank" >Script Popup</a></li>
@endif-->
<li class="dropdown">
	<a data-toggle="dropdown" href="#" style="color:#fff;"><span class="fa fa-user"></span> &nbsp; {{ Auth::user()->email }} <span class="caret"></span></a>
    <ul class="dropdown-menu" style="right:0 !important;width: 160px;left:auto !important">
    <li><a href="{{ url('admin/reset/'.Auth::user()->id) }}"><i class="fa fa-unlock"></i> Reset Password</a></li>
    @if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
        
        <!-- <li><a style="color:#fff;" href="http://172.16.160.31/lembaga/admin/parameter?i={{ Auth::user()->id_lembaga }}" target="_blank" >Setting Web Service</a></li> -->
    @endif
    <!-- <li><a style="color:#fff;" href="http://172.16.160.31/lembaga/?i={{ Auth::user()->id_lembaga }}" target="_blank" >Script Popup</a></li> --> 
    <li><a href="{{ route('signout') }}"><i class="fa fa-power-off"></i> Logout</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form></li>
    </ul>
</li>
</ul>
@endif
</nav>
</div>
</header>

    
</div>

@yield('content')
<footer>
<div class="container">
<ul class="footer-action">
<li><a id="top" class="button small" href="#">Back to top<i class="top"></i></a></li>
</ul>
<p class="copyright"><?php echo date('Y') ?> &copy; Kementerian Dalam Negeri</p>
</div>
</footer>


<script src="{{ url('/js/prototype.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/scriptaculous.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/sizzle.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/smoothscroll.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/jquery.easing.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/jquery.scrollto.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/jquery.localscroll.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/jquery.bxslider.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/waypoints.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/notifications.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/js/init.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('/jqueryui/external/jquery/jquery.js') }}"></script>
<script src="{{ url('/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/buttons.flash.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/jszip.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/pdfmake.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/vfs_fonts.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ url('/plugins/bower_components/datatables/buttons.print.min.js') }}"></script>
<script src="{{ url('/plugins/slider/js-image-slider.js') }}" type="text/javascript"></script>
<script src="{{ url('/jqueryui/jquery-ui.min.js') }}"></script>
<script src="{{ url('/js/highcharts.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/exporting.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
		$("#wrapper").toggleClass("toggled");
        $("body").toggleClass("toggled");
        $(".date").datepicker({"dateFormat": "yy-mm-dd"});
        $("#btdownload").hide();
        $("#btimport").hide();
            
        $("#cbtype").change(function(e) {
            var val = $(this).val();
            $("#btimport").show();
            if (val == "xls") {
                $("#btdownload").hide();
                $("#frmupload").attr("action", "/databalikan/sendfile");
            } else if (val == "csv") {
                $("#btdownload").hide();
                $("#frmupload").attr("action", "/databalikan/sendcsv");
            } else if (val == "") {
                $("#btdownload").hide();
                $("#btimport").hide();
            }
        });

        $("#logo,.sidebar-brand").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $("body").toggleClass("toggled");
        });
    })
</script>
@yield('custom-css-js')

</body>
</html>

