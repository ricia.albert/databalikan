@extends('layouts.template')
@section('title', 'Edit Data Lembaga')
@section('content')
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">&nbsp;</h4> </div>
    	<!-- /.col-lg-12 -->
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">&nbsp;</h3>
                        <h3 class="box-title">Edit Data Lembaga</h3>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('listlembaga/edit') }}" method="post">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <input type="hidden" id="id" name="id" value="{{ $detail->id_lembaga }}" />
                  <label for="urlpost">Lembaga Pengguna</label>
                  <input type="text" class="form-control" name="nama_lembaga" required id="nama_lembaga" placeholder="Lembaga Pengguna" value="{{$detail->lembaga_pengguna}}" />
                  @if ($errors->has('nama_lembaga'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_lembaga') }}</strong>
                        </span>
                   @endif
                </div>
                <div class="form-group">
                  <input type="hidden" id="hid_jenis" name="hid_jenis" value="{{ $detail->jenis_lembaga }}" />
                  <label for="Lembaga">Jenis Lembaga</label>
                  	<select name="jenis_lembaga" onchange="updateHidden()" required  class="form-control" id="jenis_lembaga">
                      <option value="">Pilih Jenis Lembaga</option>
                      @foreach($jenis as $key => $data)
                    	  <option value="{{$data->id_jl}}" <?php echo ($data->jenis_lembaga == $detail->jenis_lembaga) ? "selected" : "" ?> >{{$data->jenis_lembaga}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('jenis_lembaga'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenis_lembaga') }}</strong>
                        </span>
                   @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{ url('listlembaga') }}" class="btn btn-danger">Batal</a>
              </div>
            </form>
          </div>
          </div>
          </section>
    <script type="text/javascript">
      function updateHidden() {
        var val = $("#jenis_lembaga option:selected").text();
        $("#hid_jenis").val(val);
      }
    </script>
@stop