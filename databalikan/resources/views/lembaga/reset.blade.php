@extends('layouts.template')
@section('title', 'Reset Password')
@section('content')
<section id="section2" class="container">
<div class="container">
	<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h2 class="page-title">&nbsp;</h2>
            <h4 class="page-title">&nbsp;</h4>
         </div>
            <!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@yield('title')</div>
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif  
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ url('users/reset') }}">
                        {{ csrf_field() }}
                        {!! Form::hidden('id_lembaga', $detail->id_lembaga, ['id'=>'id_lembaga']) !!}
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">ID Login</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="x_lembaga" id="x_lembaga" placeholder="ID Lembaga" disabled readonly="readonly" maxlength="6" value="{{ $detail->name }}" >
                            </div>
                        </div>
						<div class="form-group">
                          <label for="nama" class="col-md-4 control-label">Nama</label>
                          <div class="col-md-6">
                          		<input type="text" class="form-control" name="lembaga_pengguna" id="lembaga_pengguna" placeholder="Lembaga Pengguna" disabled readonly="readonly" value="{{ $detail->email }}">
                           </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password Baru</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Minimal 8 karakter" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password Baru</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
