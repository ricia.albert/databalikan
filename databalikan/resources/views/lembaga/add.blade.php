@extends('layouts.template')
@section('title', 'Add Data Lembaga')
@section('content')
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">&nbsp;</h4> </div>
    	<!-- /.col-lg-12 -->
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">&nbsp;</h3>
                        <h3 class="box-title">Add Data Lembaga</h3>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('listlembaga/create') }}" method="post">
            {{ csrf_field() }}
              <div class="box-body">
                <!-- <div class="form-group">
                  <label for="urlpost">Lembaga Pengguna</label>
                  <input type="text" class="form-control" name="nama_lembaga" required id="nama_lembaga" value="{{ old('nama_lembaga') }}" placeholder="Lembaga Pengguna" >
                  @if ($errors->has('nama_lembaga'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_lembaga') }}</strong>
                        </span>
                   @endif
                </div> -->
                <div class="form-group">
                  <label for="urlpost">Nama Lembaga Pengguna</label>
                  <input type="text" class="form-control" name="email" id="email" required placeholder="Nama Lembaga Pengguna" value="{{ old('email') }}" >
                   @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                   @endif
                </div>
                <div class="form-group">
                  <input type="hidden" id="hid_jenis" name="hid_jenis" />
                  <label for="Lembaga">Jenis Lembaga</label>
                  	<select name="jenis_lembaga" onchange="updateHidden()" required  class="form-control" id="jenis_lembaga">
                      <option value="">Pilih Jenis Lembaga</option>
                      @foreach($jenis as $key => $data)
                    	  <option value="{{$data->id_jl}}">{{$data->jenis_lembaga}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('jenis_lembaga'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenis_lembaga') }}</strong>
                        </span>
                   @endif
                </div>
              </div>
              <div class="form-group">
                  <label for="urlpost">Password</label>
                  <input type="password" class="form-control" name="password" id="password" maxlength="8" placeholder="Password Lembaga Pengguna" >
                  @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                   @endif
                </div>
                <div class="form-group">
                    <label for="password-confirm">Confirm Password</label>
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password Lembaga Pengguna" required>
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{ url('listlembaga') }}" class="btn btn-danger">Batal</a>
              </div>
            </form>
          </div>
          </div>
          </section>
    <script type="text/javascript">
      function updateHidden() {
        var val = $("#jenis_lembaga option:selected").text();
        $("#hid_jenis").val(val);
      }
    </script>
@stop