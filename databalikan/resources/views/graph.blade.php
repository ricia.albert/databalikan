@extends('layouts.template')
@section('content')
<section id="section1">
<div class="sixteen columns text-center" style="margin-top: 100px"><h3>Statistic Graph Report Data</h3></div>
</section>
<section id="section2" class="overview container">
<form action="{{ url('/time') }}" method="GET">
	<div class="pull-left">
		<button type="button" id="btExport" class="btn btn-primary"><span class="fa fa-excel"></span> &nbsp;Export Excel</button>
		<div style="padding:36px 12px 0px 12px">

			Total : <strong>{{number_format($total,0,",",".")}} data</strong><br/>
		</div>
	</div>
	
	<div class="pull-right searchbox">
		<table>
			<tr>
				<td>Tanggal : </td>
				<td width="150px"><input type="text" name="sd" id="sd" class="form-control date" value="{{ $sd }}" placeholder="Tgl Mulai.." /></td>
				<td width="30px"> - </td>
				<td width="150px"><input type="text" name="ed" id="ed" class="form-control date" value="{{ $ed }}" placeholder="Tgl Akhir.." /></td>
				<td width="5px"> </td>
				<td width="115px">
					<select class="form-control" id="type" name="type">
						<option value="D" <?php echo ($type == "D") ? "selected" : "" ?>>Daily</option>
						<option value="M" <?php echo ($type == "M") ? "selected" : "" ?>>Monthly</option>
						<option value="Y" <?php echo ($type == "Y") ? "selected" : "" ?>>Annually</option>
					</select>
				</td>

			</tr>
			<tr><td><br/></td></tr>
			<tr>
				<td width="100px">Lembaga :</td>
				<td colspan="3">
					<select class="form-control" id="lembaga" name="lmb">
						<option value="">- Pilih Lembaga -</option>
						<?php foreach ($lembaga as $l) : ?>
							<option value="<?php echo $l->id_lembaga ?>" <?php echo ($search == $l->id_lembaga) ? "selected" : ""?>><?php echo $l->lembaga_pengguna ?></option>
						<?php endforeach; ?>
					</select>
				</td>
				<td></td>
				<td><button type="button" id="btSearch" class="btn btn-success"><span class="fa fa-search"></span> &nbsp;Cari Data</button></td>
			</tr>
		</table>
	</div>
</form>
<div class="chart">
  	<!-- Hidden Values -->
	<div id="jsonValue"  style="display: none"><?php echo $value ?></div>
	<div id="timeStamp" style="display: none"><?php echo $time ?></div>
	<!-- Sales Chart Canvas -->
	<div id="chartsa" style="min-width: 310px; height: 400px; margin: 140px auto 0px auto"></div>
</div>

<table class="table" style="margin-top: 45px">
	<thead>
		<tr>
			<th width="36px"><strong>No.</strong></th>
			<th style="text-align: center"><strong>Tanggal Input</strong></th>
			<th style="text-align: center"><strong>Jumlah Data</strong></th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$num = 1;
			foreach ($data as $d) : ?>
			<tr>
				<td><?php echo $num ?>.</td>
				<td>
					<?php 
						if ($type == "D")
							echo date('d M Y', strtotime($d->dates));
						else if ($type == "M")
							echo date('M Y', strtotime($d->dates));
						else if ($type == "Y")
							echo "Tahun ".$d->dates;
					?>		
				</td>
				<td><strong><?php echo number_format($d->jml) ?> &nbsp;Data</strong></td>
			</tr>
		<?php
			$num++; 
			endforeach; ?>
	</tbody>
</table>

</section>

<script type="text/javascript">
	var jsonValueV = $.parseJSON($("#jsonValue").html());
	var timeStamp = $.parseJSON($("#timeStamp").html());
	$(document).ready(function(e) {
		$("#btSearch").click(function(e) {
			var sd = $("#sd").val();
			var ed = $("#ed").val();
			var lmb = $("#lembaga").val();
			var type = $("#type").val();
			if (lmb != "") {
				window.location = "graph?lmb=" + lmb + "&sd="+sd+"&ed="+ed+"&type="+type;	
			} else {
				alert("Harap memilih lembaga terlebih dahulu.");
			}
		});

		$("#btExport").click(function(e) {
			var sd = $("#sd").val();
			var ed = $("#ed").val();
			var lmb = $("#lembaga").val();
			var type = $("#type").val();
			if (lmb != "") {
				window.location = "graph/excel?lmb=" + lmb + "&sd="+sd+"&ed="+ed+"&type="+type;	
			} else {
				alert("Harap memilih lembaga terlebih dahulu.");
			}
		});
	});
	$(function () {
    	Highcharts.chart('chartsa', {
    		chart: {
    			type: 'line'
    		},
    		title: {
    			text: 'Data Statistic Chart'
    		},
    		subtitle: {
    			text: 'Source: Data Balikan'
    		},
    		xAxis: {
    			categories: timeStamp
    		},
    		yAxis: {
    			title: {
    				text: 'Jumlah Data'
    			}
    		},
    		plotOptions: {
    			line: {
    				dataLabels: {
    					enabled: true
    				},
    				enableMouseTracking: false
    			}
    		},
    		series: [{
    			name: 'Jumlah Data',
    			data: jsonValueV
    		}]
    	});
    });
</script>

@endsection