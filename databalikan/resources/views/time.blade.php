@extends('layouts.template')
@section('content')
<section id="section1">
<div class="sixteen columns text-center" style="margin-top: 100px"><h3>Report Databalikan Lembaga</h3>(Detail Yang Ditampilkan Max 5 data)</div>
</section>
<section id="section2" class="overview container">
<form action="{{ url('/time') }}" method="GET">
	<div class="pull-left">
		<a href="{{ url('/time/excel') }}?sd={{ $sd }}&ed={{ $ed }}&s={{ $search }}"><button type="button" class="btn btn-primary"><span class="fa fa-excel"></span> &nbsp;Export Excel</button></a>
		<div style="padding:36px 12px 0px 12px">

			Total : <strong>{{number_format($total,0,",",".")}} data</strong><br/>
		</div>
	</div>
	
	<div class="pull-right searchbox">
		<table>
			<tr>
				<td>Tanggal : </td>
				<td width="150px"><input type="text" name="sd" id="sd" class="form-control date" value="{{ $sd }}" placeholder="Tgl Mulai.." /></td>
				<td width="30px"> - </td>
				<td width="150px"><input type="text" name="ed" class="form-control date" value="{{ $ed }}" placeholder="Tgl Akhir.." /></td>
				<td></td>

			</tr>
			<tr><td><br/></td></tr>
			<tr>
				<td width="100px">Search :</td>
				<td colspan="3">
					<input type="text" name="s" class="form-control" value="{{ $search }}" placeholder="Cari disini.." />
				</td>
				<td width="80px"><button type="submit" class="btn btn-success"><span class="fa fa-search"></span> &nbsp;Cari</button></td>
			</tr>
		</table>
	</div>
</form>
<?php $no = 1; ?>
<table class="table">
	<thead>
		<tr>
			<th width="30px" class="text-center"><strong>No.</strong></th>
			<th width="15%" class="text-left"><strong>ID Lembaga</strong></th>
			<th width="40%" class="text-left"><strong>Lembaga Pengguna</strong></th>
			<th width="15%" class="text-right"><strong>Jumlah Data</strong></th>
			<th width="110px" class="text-center"><strong>Terakhir Kirim</strong></th>
			<th width="1px"></th>
		</tr>
	</thead>
	<tbody>
	@foreach($lembaga as $key => $data)
		<tr>
			<td>{{$no}}</td>
			<td class="text-left" ><strong>{{ $data->name }}</strong></td>
			<td class="text-left"><strong>{{ $data->email }}</strong></td>
			<td class="text-right"><strong>{{ number_format($data->datacount,0,",",".") }}</strong></td>
			<td class="text-center">{{ \Carbon\Carbon::parse($data->last_send)->format('d M Y H:i') }}</strong></center></td>
			<td class="text-center"><input type="button" class="btn btn-primary" id="{{$data->id_lembaga}}" name="{{$data->id_lembaga}}" value="Detail" onClick="lClick(this)"/></td>
		</tr>
		<tr>
		<td colspan="6"><div id="div_{{$data->id_lembaga}}" style="display:none;"></div></td>
		</tr>
	<?php $no++ ?>
	@endforeach
	</tbody>
</table>

</section>

@endsection

<script>
function lClick(e)
{
	$("#div_"+e.id).toggle('show');
	$("#div_"+e.id).load("/databalikan/databalikan/databalikan/time/ajaxTime?id_lembaga="+e.id+"&sd="+$("#sd").val()+"&ed="+$("#ed").val());
}
</script>