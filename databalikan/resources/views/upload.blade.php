@extends('layouts.app')
@section('title', 'Upload Data')
@section('content')
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">@yield('title')</h4> </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">Upload Data Lembaga</h3>
            <form class="form-horizontal">
            	<div class="form-group">
                    <label class="col-md-12">Tipe Data</label>
                    <div class="col-md-6">
                		<input type="radio" value="1" name="tipe" /> Upload from Excel
                        <input type="radio" value="2" name="tipe" /> Upload from DMP
					</div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Pilih Lembaga</label>
                    <div class="col-md-12">
                	{{ Form::file('file_upload') }}	
                     </div>
                </div>
            </form>
		</div>
    </div>
</div>
</div>
@stop
@section('custom-css-js')
@stop