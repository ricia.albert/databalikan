@extends('layouts.template')
@section('title', 'Master Jenis Lembaga')
@section('content')
    <section id="section2" class="container">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                	<h3 class="box-title">&nbsp;</h3>
                	<h3 class="box-title"><a href="{{ url('/listlembaga/create') }}" class="btn btn-info btn-primary btn-sm"><i class="fa fa-binoculars"></i> Create</a></h3>
                    <h3 class="box-title">Master Jenis Lembaga</h3>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif                    
                	<div class="table-responsive">
                        <br>
                        <table id="myTable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Jenis</th>
                                <th>Jenis Lembaga</th>
                                <th>Last Updated</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result as $key=>$value)
                                <tr>
                                    <td>{!! $key+1 !!}</td>
                                    <td>{!! $value->id_jenis !!}</td>
                                    <td>{!! $value->lembaga_pengguna !!}</td>
                                    <td>{!! $value->jenis_lembaga !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@stop
@section('custom-css-js')
    <script>
        (function($) {
            $('#myTable').DataTable({
                "pageLength": 50
            });
        }) (jQuery);
    </script>
@stop