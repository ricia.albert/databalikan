<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="{{ url('/plugins/images/admin-logo.png') }}">
<title>Aplikasi Data Balikan v.1.0</title>
<!-- Bootstrap Core CSS -->
<link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- animation CSS -->
<link href="{{ url('/css/animate.css') }}" rel="stylesheet">
<link href="{{ url('/css/style_app.css') }}" rel="stylesheet">
<link href="{{ url('/css/colors/blue.css') }}" id="theme"  rel="stylesheet">

</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
<div class="login-box">
    <div class="white-box">
    	<h3 align="center">Direktorat Jenderal<br>Kependudukan dan Pencatatan Sipil<br>
        Kementerian Dalam Negeri</h3>
      	<form class="form-horizontal form-material" method="POST" id="loginform" action="{{ route('login') }}">
		<a href="javascript:void(0)" class="text-center db"><img src="{{ url('plugins/images/adminlogo.png') }}"width="150"><br/>
       	</a>  
		<h3 class="box-title m-b-0" align="center">Login</h3>
		{{ csrf_field() }}
        <div class="form-group  m-t-20{{ $errors->has('name') ? ' has-error' : '' }}">
          <div class="col-xs-12">
            <label>Username</label>
            <input id="name" type="text" class="form-control" placeholder="Username" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <div class="col-xs-12">
            <label>Password</label>
            <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Masuk</button>
          </div>
        </div>
      </form>
      <div class="clearfix"></div>
      <div class="clearfix"></div>
      <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <label>Aplikasi Data Balikan v.1.0</label>
          </div>
        </div>
    </div>
  </div>
  </section>
<!-- jQuery -->
<script src="{{ url('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ url('/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ url('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ url('/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ url('/js/waves.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ url('/js/custom.min.js') }}"></script>
<!--Style Switcher -->
<script src="{{ url('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
</body>
</html>
