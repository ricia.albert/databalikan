@extends('layouts.template')
@section('title', 'Update Token Lembaga')
@section('content')
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">&nbsp;</h4> </div>
    <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">&nbsp;</h3>
                <h3 class="box-title">@yield('title')</h3>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(array('method'=>'post','url'=>'users/token/','id'=>'token','novalidate')) !!}
                  <div class="box-body">
                    <div class="form-group">
                      <label for="url">ID Lembaga</label>
                      {!! Form::hidden('id_lembaga', $detail->id_lembaga, ['id'=>'id_lembaga']) !!}
                      <input type="text" class="form-control" name="x_lembaga" id="x_lembaga" placeholder="ID Lembaga" disabled readonly="readonly" maxlength="6" value="{{ $detail->name }}" >
                    </div>
                    <div class="form-group">
                      <label for="urlpost">Nama Lembaga</label>
                      <input type="text" class="form-control" name="lembaga_pengguna" id="lembaga_pengguna" placeholder="Lembaga Pengguna" disabled readonly="readonly" value="{{ $detail->email }}">
                    </div>
					<div class="form-group">
                      <label for="Lembaga">Link Web Service (Get Parameter Lembaga)</label>
                      <input type="text" id="link" name="link" class="form-control" readonly="readonly" value="{{ Config('app.get_param') }}">
                    </div>
                    <div class="form-group">
                      <label for="Lembaga">Link Web Service (Kirim Data Balikan)</label>
                      <input type="text" id="link" name="link" class="form-control" readonly="readonly" value="{{ Config('app.store') }}">
                    </div>
                    <div class="form-group">
                      <label for="Lembaga">API Token</label>
                      <textarea id="api_token" name="api_token" class="form-control" readonly="readonly">{{ $detail->api_token }}</textarea>
                    </div>
                    <div class="form-group">
                      <input type="button" id="reset" name="reset" class="btn btn-primary" onclick="generate();" value="Reset">
                    </div>
                  </div>
                  <!-- /.box-body -->
                
                  <div class="box-footer">
                    <button type="submit" name="simpan" class="btn btn-success">Simpan</button>
                    <a href="{{ url('users') }}" class="btn btn-danger">Batal</a>
                  </div>
                {!! Form::close() !!}
	        </div>
        </div>
     </div>
    </div>
</section>
@stop
@section('custom-css-js')
<script type="text/javascript">
function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
function generate()
{
	api_token.value = randomPassword(100);
}
</script>
@stop