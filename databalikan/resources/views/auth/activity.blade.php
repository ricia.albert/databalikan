@extends('layouts.template')
@section('title', 'List Activity Log')
@section('content')
    <section id="section2" class="container">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                	<h3 class="box-title">&nbsp;</h3>
                	<h3 class="box-title">&nbsp;</h3>
                    <!--  <form action="{{ url('/time') }}" method="GET">
                        <div class="pull-right searchbox">
                            <table>
                                <tr>
                                    <td>Tanggal : </td>
                                    <td width="150px"><input type="text" name="sd" id="sd" class="form-control date" placeholder="Tgl Mulai.." /></td>
                                    <td width="30px" align="center"> - </td>
                                    <td width="150px"><input type="text" name="ed" class="form-control date" placeholder="Tgl Akhir.." /></td>
                                </tr>
                            </table>
                        </div>
                    </form>  -->
                    <h3 class="box-title">@yield('title')</h3>
                   
                    <div class="table-responsive" style="width: 100%">
                        <br>
                        <table id="myTable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Users</th>
                                <th>Content Type</th>
                                <th>Action</th>
                                <th>Description</th>
                                <th>Details</th>
                                <th>IP Address</th>
                                <th>Created Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@stop
@section('custom-css-js')
    <script>
        (function($) {
            $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ordering: true,
                ajax: "{{ url('/admin/getactivity') }}",
                columns: [
                    {data: 'nomor', name: 'nomor', className: 'text-center'},
                    {data: 'name', name: 'users.name'},
                    {data: 'content_type', name: 'content_type'},
                    {data: 'action', name: 'action'},
                    {data: 'description', name: 'description'},
                    {data: 'details',name:'details'},
                    {data: 'ip_address',name:'ip_address'},
                    {data: 'created_at', name: 'created_at'}
                ],
                "pageLength": 50
            });
        }) (jQuery);
    </script>
@stop