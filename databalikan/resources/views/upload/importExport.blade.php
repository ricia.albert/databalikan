@extends('layouts.template')
@section('title', 'Upload Data')
@section('content')
<section id="section1" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
   		<h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h4 class="page-title">Upload Data Balikan</h4> </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading"><p style="font-size: x-large;">{{$nama}}</p></div>
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif 
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    
                    <form action="{{ URL::to('uploadPost') }}" id="frmupload" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="form-body">
                            <!--<h3 class="box-title">Person Info</h3>
                            <hr>-->
                            {{ csrf_field() }}
                                <?php if (isset($_GET['scs'])) : ?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <strong>Success!</strong> File telah berhasil terupload &amp; diproses.
                                    </div>
                                <?php endif; ?>

                                <?php if (isset($_GET['err'])) : ?>
                                    <?php if ($_GET['err'] == 1) { ?>
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <strong>Error!</strong> File gagal diupload.
                                        </div>
                                    <?php } else if ($_GET['err'] == 2) { ?>
                                         <div class="alert alert-danger alert-dismissible" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <strong>Error!</strong> Ukuran file melebihi 12 MB.
                                        </div>
                                    <?php } ?>
                                <?php endif; ?>
                                <div class="row" >
                                        <div class="form-group">
                                            <label style="text-align: left" class="col-md-offset-1 col-md-2 control-label">Jenis File</label>
                                            <div style="text-align: left" class="col-md-3">
                                                <select class="form-control" id="cbtype">
                                                    <option value="">- Pilih Jenis Upload -</option>
                                                    <option value="xls">File Excel</option>
                                                    <option value="csv">File CSV</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label style="text-align: left" class="col-md-offset-1 col-md-2 control-label">Upload File</label>
                                            <div style="text-align: left" class="col-md-2">{{ csrf_field() }}
                                            {{ Form::file('fnupload') }}</div>
                                        </div>
                                        <div class="form-group">
                                            
                                            <div class="col-md-offset-1 col-md-5"><a href="{{ URL::to('downloadExcel/xls') }}"><button type="button" id="btdownload" style="width:100%" class="btn btn-success">Download Template Form Excel</button></a><br/>
                                            <span style="color:red;font-size: 10pt" class="pull-right">*Ukuran maksimum file adalah 12 MB.</span>
                                        	<button class="btn btn-primary" id="btimport" style="width:100%">Import File</button>
                                        </div>
                                </div>
                                </div>
                        
                                
                            <!--/row-->
                        </div>
                        
                    </form>

                </div>
            </div>
        </div>
        
        
    </div>
 </div>
 </div>
 </section>
@stop