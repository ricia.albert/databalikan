@extends('layouts.template')
@section('title', 'Detail Data')
@section('content')
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h3 class="page-title">&nbsp;</h3> </div>
    <!-- /.col-lg-12 -->
	</div>
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h3 class="page-title">&nbsp;</h3> </div>
    <!-- /.col-lg-12 -->
	</div>
<!--START AREA ATAS-->
@foreach($detail->content as $result)
<div class="row">
<div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading">{!! $result->NAMA_LGKP !!}</div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div class="panel-body">
                <form action="#">
                    <div class="form-body">
                        <!--<h3 class="box-title">Person Info</h3>
                        <hr>-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    NIK<br />
                                    <label class="control-label">{!! $result->NIK !!}</label> </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    Status Kawin<br />
                                    <label class="control-label">{!! $result->STATUS_KAWIN !!} </label></div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                  	 Alamat<br />
                                    <label class="control-label">{!! $result->ALAMAT !!}</label> </div>
                            </div>
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    Jenis Kelamin<br />
                                    <label class="control-label">{!! $result->JENIS_KLMIN !!}</label> </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    Tanggal Kawin<br />
                                    <label class="control-label">{!! $result->TGL_KWN !!}</label> </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                	Desa/Kelurahan<br />
                                    <label class="control-label">{!! $result->KEL_NAME !!}</label> </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    Tempat Lahir<br />
                                    <label class="control-label">{!! $result->TMPT_LHR !!} </label></div> 
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    Status Hubungan Keluarga<br />
                                    <label class="control-label">{!! $result->STAT_HBKEL !!}</label> </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                	Kecamatan<br />
                                    <label class="control-label">{!! $result->KEC_NAME !!} </label></div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    Tanggal Lahir<br />
                                    <label class="control-label">{!! $result->TGL_LHR !!}</label> </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    No Akta Lahir<br />
                                    <label class="control-label">{!! $result->NO_AKTA_LHR !!}</label> </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                	Kabupaten<br />
                                    <label class="control-label">{!! $result->KAB_NAME !!}</label> </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    No KK<br />
                                    <label class="control-label">{!! $result->NO_KK !!}</label> </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    Status KTP Elektronik<br />
                                    <label class="control-label">{!! $result->EKTP_STATUS !!} </label></div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                	Provinsi<br />
                                    <label class="control-label">{!! $result->PROP_NAME !!}</label> </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <!--/span-->
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                    <div class="form-actions">
                    	<button type="button" onclick="document.location.replace('{{ url('listdata') }}')" class="btn btn-default">Kembali</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
@endforeach
 <!--END AREA ATAS-->
<!--START AREA BAWAH-->
<div class="row">
<div class="col-lg-12">
<div class="table-responsive">        
    <table id="myTable" class="table table-striped">
        <tbody>
            @for($i=0;$i< count($hasil);$i++)
            <tr>
                <td class="col-lg-3">{!! $hasil[$i]->lembaga_pengguna !!}</td>
                @foreach($lembaga[$i] as $item=>$itemval)
                @if ($itemval->parameter=="NIK")
                @else
                	<td >{!! $itemval->parameter !!} : {!! $itemval->value !!}</td>
                @endif
                @endforeach
            </tr>
            @endfor	
        </tbody>
    </table>
</div>              
</div>
</div>
<!--END AREA BAWAH-->
</div>
</section>
@stop