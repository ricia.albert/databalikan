@extends('layouts.template')
@section('content')
<section id="section1" class="hero container">
<div><h2>&nbsp;</h2></div>
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
<div class="text-center">	
	<h3>Selamat datang, {{ Auth::user()->email }}</h3>	
</div>
<div class="clear"></div>
<div id="sliderFrame">
    <div id="slider">
        @foreach($sliders as $key=>$value) 
            <img src="images/{{ $value->slider_path }}" width="100%" />
        @endforeach
    </div>
    <div id="htmlcaption" style="display: none;">
        <em>Silahkan pilih layanan di bawah </em><a href="#">ini</a>.
    </div>
</div>
<ul class="action">
<li><h2>Direktorat Jenderal Kependudukan dan Pencatatan Sipil<br />
Kementerian Dalam Negeri<h2></li>
</ul>
<div class="clear"></div>
@if(Auth::user()->id_lembaga != "ADMIN" && Auth::user()->id_lembaga != "SUBADM")
<section id="section2" class="overview container">
	<center>
		<div class="col-md-12 panel-dashboard2" style="background-color: #2196F3;">
			<h4 style="color:#fff">Total Data berhasil diupload</h4>
		    <h3 style="color:#fff"><strong>{{ Auth::user()->email }}</strong></h3>
		    <h1 class="no-margin-top" style="color:#fff"><strong>{{ number_format($data->datacount,0,",",".") }}</strong></h1>
		    <center style="font-size: 12pt" style="color:#fff">Terakhir : <strong>{{ \Carbon\Carbon::parse($data->last_send)->format('d M Y H:i') }}</strong></center>
		</div>
        <div class="col-md-12 panel-dashboard2" style="background-color: #00BCD4;">
            <h4 style="color:#fff">Data berhasil diupload hari ini</h4>
            <h3 style="color:#fff"><strong>{{ Auth::user()->email }}</strong></h3>
            <h1 class="no-margin-top" style="color:#fff"><strong>{{ number_format(($current == null) ? 0 : $current->datacount,0,",",".") }}</strong></h1>
            <center style="font-size: 12pt" style="color:#fff">Tanggal : <strong>{{ \Carbon\Carbon::parse(date('Y-m-d'))->format('d M Y') }}</strong></center>
        </div>
	</center>
</section>
@endif
</section>

<section id="section2" class="overview container">

<h3>Layanan Aplikasi Data Balikan</h3>
<br class="clear"/>



@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM" || Auth::user()->id_lembaga == "USERS")
<a href="{{ url('listdata') }}"><div class="content-box col-md-4">
<img src="{{ url('images/design.png') }}" alt="">
<h3>Data - Data Penduduk</h3>
<p></p>
</div></a>
@endif
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
<a href="{{ url('dashboard') }}">
<div class="content-box col-md-4">
<img src="{{ url('images/setup.png') }}" alt="">
<h3>Dashboard Lembaga</h3>
<p></p>
</div></a>
<a href="{{ url('graph') }}">
<div class="content-box col-md-4">
<img src="{{ url('images/documentation.png') }}" alt="">
<h3>Grafik Statistik</h3>
<p></p>
</div>
</a>
@elseif(Auth::user()->id_lembaga != "USERS")
<a href="{{ url('setParameter') }}"><div class="content-box col-md-4">
<img src="{{ url('images/design.png') }}" alt="">
<h3>Setting Data Balikan</h3>
<p></p>
</div></a>
<a href="{{ url('upload') }}">
<div class="content-box col-md-4">
<img src="{{ url('images/setup.png') }}" alt="">
<h3>Upload Data Balikan</h3>
<p></p>
</div></a>
<a href="{{ url('form') }}">
<div class="content-box col-md-4">
<img src="{{ url('images/documentation.png') }}" alt="">
<h3>Form Input Data Balikan</h3>
<p></p>
</div>
</a>
@endif
</section>
@endsection
