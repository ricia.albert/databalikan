@extends('layouts.template')
@section('content')
<section id="section1" class="hero container">
<div><h2>&nbsp;</h2></div>
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
<div class="sixteen columns text-center"><h3>Login sebagai {{ Auth::user()->email }}</h3></div>
<ul class="action sixteen columns">
<li><h2>Direktorat Jenderal Kependudukan dan Pencatatan Sipil<br />
Kementerian Dalam Negeri<h2></li>
</ul>
</section>
<section id="section2" class="overview container">
<h3 class="sixteen columns">Layanan Aplikasi Data Balikan</h3>
<br class="clear">

@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM" || Auth::user()->id_lembaga == "USERS")
<a href="{{ url('listdata') }}"><div class="content-box one-third column">
<img src="{{ url('images/design.png') }}" alt="">
<h3>Data Penduduk</h3>
<p></p>
</div></a>
@endif
@if(Auth::user()->id_lembaga == "ADMIN" || Auth::user()->id_lembaga == "SUBADM")
<a href="{{ url('listlembaga') }}">
<div class="content-box one-third column">
<img src="{{ url('images/setup.png') }}" alt="">
<h3>Data Lembaga</h3>
<p></p>
</div></a>
<a href="{{ url('/admin/activity') }}">
<div class="content-box one-third column">
<img src="{{ url('images/documentation.png') }}" alt="">
<h3>Log System</h3>
<p></p>
</div>
</a>
@elseif(Auth::user()->id_lembaga != "USERS")
<a href="{{ url('setParameter') }}"><div class="content-box one-third column">
<img src="{{ url('images/design.png') }}" alt="">
<h3>Setting Data Balikan</h3>
<p></p>
</div></a>
<a href="{{ url('upload') }}">
<div class="content-box one-third column">
<img src="{{ url('images/setup.png') }}" alt="">
<h3>Upload Data Balikan</h3>
<p></p>
</div></a>
<a href="{{ url('form') }}">
<div class="content-box one-third column">
<img src="{{ url('images/documentation.png') }}" alt="">
<h3>Form Input Data Balikan</h3>
<p></p>
</div>
</a>
@endif
</section>
@endsection
