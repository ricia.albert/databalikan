@extends('layouts.template')
@section('content')
<section id="section1">
<div class="sixteen columns text-center" style="margin-top: 100px"><h3>Dashboard Data</h3></div>
<ul class="action sixteen columns" style="margin-top: 0px; padding-top: 0px">
<li><h3>Direktorat Jenderal Kependudukan dan Pencatatan Sipil<br />
Kementerian Dalam Negeri<h3>
	<h3><strong>Total Data Balikan : {{number_format($total,0,",",".")}} Data</strong></h3>
<h3><strong>Total Lembaga : {{number_format($totalLembaga,0,",",".")}} Lembaga</strong></h3>
<br/>
<div class="pull-right searchbox" style="padding-bottom: 0">
	<form action="{{ url('/dashboard') }}" method="GET">
		<table>
			<tr>
				<td width="120px"><strong>Lembaga : </strong></td>
				<td width="380px"><input type="text" name="s" value="{{ $search }}" class="form-control" value="" placeholder="Cari disini.." /></td>
				<td width="80px"><button type="submit" class="btn btn-success"><span class="fa fa-search"></span> &nbsp;Cari</button></td>
			</tr>
		</table>
	</form>
</div>
</li>
<li>
	
</li>
</ul>

</section>

<section id="section2" class="overview container">
<center>
<?php $i = 0; ?>
@foreach($lembaga as $key => $data)
    
    <div class="col-md-3 panel-dashboard" style="background-color: <?php 
    	if ($i % 2 == 0)
    		echo "#00BCD4";
    	else
    		echo "#2196F3";

    ?>">
        <h4 style="font-size: 1em;color: #fff;"><strong>{{$data->email}}</strong></h4>
        <h3 class="no-margin-top" style="color: #fff;"><strong>{{ number_format($data->datacount,0,",",".") }}</strong></h3>
        <center style="font-size: 10pt">Terakhir : <strong>{{ \Carbon\Carbon::parse($data->last_send)->format('d M Y H:i') }}</strong></center>
        <br/>
        <a style='color:white;font-size: 10pt;text-decoration: underline' href="{{ url('/graph?lmb='.$data->id_lembaga) }}"><strong><span class="fa fa-line-chart"></span> &nbsp;Lihat Statistik..</strong></a>
    </div>
    <?php $i++ ?>
@endforeach
</center>
</section>
@endsection
