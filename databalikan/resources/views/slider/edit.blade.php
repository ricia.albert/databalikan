@extends('layouts.template')
@section('title', 'Edit Data Lembaga')
@section('content')
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">&nbsp;</h4> </div>
    	<!-- /.col-lg-12 -->
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">&nbsp;</h3>
                        <h3 class="box-title">Ubah Gambar Slider</h3>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('listslider/edit') }}" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <br/><br/>
              <input type="hidden" id="id" name="id" value="{{ $detail->id_slider }}" />
              <div class="box-body">
                <div class="form-group">
                  <label for="urlpost">Image Slider</label>
                  <input type="file" name="fnimage" id="fnimage" />
                </div>
              </div>
              <br/>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{ url('listslider') }}" class="btn btn-danger">Batal</a>
              </div>
            </form>
          </div>
          </div>
          </section>
    <script type="text/javascript">
      function updateHidden() {
        var val = $("#jenis_lembaga option:selected").text();
        $("#hid_jenis").val(val);
      }
    </script>
@stop