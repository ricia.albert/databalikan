@extends('layouts.template')
@section('title', 'List Slider')
@section('content')
    <section id="section2" class="container">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">&nbsp;</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                	<h3 class="box-title">&nbsp;</h3>
                	<h3 class="box-title"><a href="{{ url('/listslider/create') }}" class="btn btn-info btn-primary btn-sm"><i class="fa fa-binoculars"></i> Create Slider</a></h3>
                    <h3 class="box-title">List Slider</h3>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif                    
                	<div class="table-responsive">
                        <br>
                        <table id="myTable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>File Name</th>
                                <th>Image Slider</th>
                                <th width="140px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result as $key=>$value)
                                <tr>
                                    <td>{!! $key+1 !!}</td>
                                    <td>{!! $value->slider_path !!}</td>
                                    <td>
                                        <img src="images/{!! $value->slider_path !!}" width="360px" />
                                    <td>
                                        <a href="listslider/edit?id={!! $value->id_slider !!}"><button type="button" class="btn btn-warning">Edit</button></a>
                                        <button type="button" data-name="{!! $value->slider_path !!}" data-id="{!! $value->id_slider !!}" class="remove btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@stop
@section('custom-css-js')
    <script>
        (function($) {
            $('#myTable').DataTable({
                "pageLength": 50
            });

            $("body").on("click", ".remove", function(e) {
                var name = $(this).attr("data-name")
                var conf = confirm("Apa anda yakin ingin hapus " + name + " ?");
                if (conf) {
                    var id = $(this).attr("data-id");
                    window.location = "listslider/remove?id=" + id;
                }
            });
        }) (jQuery);
    </script>
@stop