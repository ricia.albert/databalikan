@extends('layouts.template')
@section('title', 'List Data')
@section('content')
<style type="text/css">
.modal-header-primary {
	color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #428bca;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
}
</style>
<section id="section1" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">&nbsp;</h4>
	</div>
    <!-- /.col-lg-12 -->
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">&nbsp;</h3>
                <form method="post" action="{{ url('/listdata') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-3">
                        <!-- Pilih Source : -->
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center"> &nbsp; 
                            <select name="type" class="form-control">
                                <option value="">- Pilih Jenis Pencarian -</option>
                                <option value="nik" {{ $type == 'nik' ? 'selected' : ''}} >NIK</option>
                                <option value="value" {{ $type == 'value' ? 'selected' : ''}}>Value</option>
                            </select></div> <br />
                        <input type="text" placeholder="Inputkan Pencarian.." class="form-control"  id="nikfind" name="nikfind" maxlength="16" value="{{ Request::input('nikfind') }}" />
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3">
                    </div>
                    <div class="col-lg-6 text-center">
                    	<input type="submit" name="find" value="Cari" class="btn btn-primary radius-4" />
                    </div>
                </div>
                </form>
                @if($biodata=='Null')
                @if(Session::has('flash_message'))
                <div class="table-responsive">
                    <div class="alert alert-danger">
                        {{ Session::get('flash_message') }}
                    </div>
                </div>
                @endif
                @else
                <br>
                <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>NAMA LENGKAP</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($biodata->content as $key=>$value)
                        <tr>
                            <td>{!! $key+1 !!}</td>
                            <?php if ($type == "nik") {?>
                                <td>{!! $value->NIK !!}</td>
                                <td>{!! $value->NAMA_LGKP !!}</td>
                                <td><a href="{{ url('detaildata/'.$value->NIK ) }}" class="btn btn-info btn-primary btn-sm"><i class="fa fa-binoculars"></i> value</a></td>
                            <?php } else if ($type == "value") {  ?>
                                <td>{!! $value->nik !!}</td>
                                <td>{!! $value->nama_lgkp !!}</td>
                                <td><a href="{{ url('detaildata/'.$value->nik ) }}" class="btn btn-info btn-primary btn-sm"><i class="fa fa-binoculars"></i> value</a></td>
                            <?php } ?>
                        </tr>
                        @endforeach	
                    </tbody>
                </table>
                </div>
                @endif  
             </div>
        </div>
    </div>
</div>
</section>
@stop
@section('custom-css-js')

@stop