<?php $no = 1; ?>
<table class="table">
	<thead>
		<tr>
			<th class="text-center"><strong>No</strong></th>
			<th class="text-center"><strong>NIK</strong></th>
			<th class="text-center"></th>
		</tr>
	</thead>
	<tbody>
	@foreach($lembaga as $key => $data)
		<tr>
			<td class="text-center">{{$no}}</td>
			<td class="text-center">{{ $data->nik }}</td>
			<th class="text-center"><a href="/databalikan/databalikan/databalikan/detaildata/{{ $data->nik }}" target="_blank"><input type="button" class="btn btn-primary" value="View" /></a></th>
		</tr>
	<?php $no++ ?>
	@endforeach
	</tbody>
</table>