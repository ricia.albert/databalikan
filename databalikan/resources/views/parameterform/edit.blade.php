@extends('layouts.template')
@section('title', 'Parameter')
@section('content')
<section id="section1" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h4 class="page-title">Setting Data Balikan</h4> </div>
    <!-- /.col-lg-12 -->
	</div>
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-info">
	        <div class="panel-heading"><p style="font-size: x-large;">{{$nama}}</p></div>
	        <div class="panel-wrapper collapse in" aria-expanded="true">
	            <div class="panel-body">
	                <form action="{{ URL::to('postEditParameter') }}" class="form-horizontal" method="post">
	                    <div class="form-body">
	                       
	                        {{ csrf_field() }}
	                        <div id="place">
	                        	<?php $i=0; ?>
		                        @foreach ($prevs as $prev)
		                        @if($i==0)
		                        
		                        	<div class="row" >
		                        	    <div class="form-group">
		                        	    <div class="col-md-2 control-label">
		                        	        <label class="control-label">NIK<?php $i++; ?></label>
		                        	    </div>
		    							<div class="col-md-4">

			                                    <input type="text" name="none[]" class="form-control" value="{{$prev->parameter}}" readonly="readonly" />
		                            	</div>
								</div>	
								</div>
		                        @else
		                        	<div class="row" >
		                        	    <div class="form-group">
			                        	    <div class="col-md-2 control-label">
			                        	        <label class="control-label">Data Balikan <?php echo($i);$i++; ?></label>
			                        	    </div>
			    							<div class="col-md-4">
				                                <input type="text" name="none[]" class="form-control" value="{{$prev->parameter}}" readonly="readonly" />
			                            	</div>
			                            	<div class="col-md-2">
			                            		<a href="{{ URL::to('removeParameter') }}?p={{$prev->parameter}}">
				                                	<button type="button" class="btn btn-danger">Remove</button>
				                            	</a>
			                            	</div>
									    </div>
								    </div>
		                        @endif
		                        
								@endforeach
							</div>
	                            
	                        <!--/row-->
	                    </div>
	                    <div class="form-actions">
	                        <div class="col-md-2"></div>
	                        <div class="col-md-4">
	                            <button type="button" class="btn btn-primary" onclick="addInput();">+</button>
	                            <button class="btn btn-primary">Submit</button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
</div>

</div>
</section>
@stop

<script type="text/javascript">
	var i = {{$i}};
	function addInput(){
		var container = document.createElement('container');
    	container.innerHTML = '<div class="row" ><div class="form-group"><div class="col-md-2 control-label"><label class="control-label">Data Balikan <span class="spnum">'+i+'</span></label></div><div class="col-md-4"><input type="text" name="import_file[]" class="form-control" /></div><div class="col-md-2"><button type="button" onclick="remove(this)" class="btn btn-danger">Remove</button></div></div></div>';
    	document.getElementById('place').appendChild(container);
    	i++;
	}

	function remove(obj) {
		$(obj).parent().parent().parent().remove();
		i--;
		resort();
	}

	function resort() {
		var spnum = $(".spnum");
		var no = {{$i}};
		for(var j=0;j<spnum.length;j++) {
			$(spnum[j]).html(no);
			no++;
		}
	}
</script>
