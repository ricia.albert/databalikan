@extends('layouts.template')
@section('title', 'Parameter')
@section('content')
<section id="section1" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    	<h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h4 class="page-title">Setting Data Balikan </h4> </div>
    <!-- /.col-lg-12 -->
	</div>
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-info">
	        <div class="panel-heading"><p style="font-size: x-large;">{{$nama}}</p></div>
	        <div class="panel-wrapper collapse in" aria-expanded="true">
	            <div class="panel-body">
	            	@if($error == 1)
                		<div class="error-text">*)Parameter upload harus lebih dari 1 parameter.</div>
                	@endif
	                <form action="{{ URL::to('postParameter') }}" class="form-horizontal" method="post">
	                	
	                    <div class="form-body">
	                        <!--<h3 class="box-title">Person Info</h3>
	                        <hr>-->
	                        {{ csrf_field() }}
	                        <div id="place">
		                        <div class="row" >
	                                <div class="form-group">
		                                <div class="col-md-2 control-label">
		                                    <label class="control-label">NIK</label>
		                                </div>
		    							<div class="col-md-4">
		                                    <input type="text" class="form-control" name="import_file[]" value="NIK" readonly="readonly" />
		                            	</div>
	                            	</div>
								</div>	
							</div>
	                            
	                        <!--/row-->
	                    </div>
	                    <div class="form-actions">
	                        <div class="col-md-2"></div>
	                        <div class="col-md-4">
	                            <button type="button" class="btn btn-primary" onclick="addInput();">+</button>
	                            <button class="btn btn-primary">Submit</button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
</div>

</div>
</section>
@stop

<script type="text/javascript">
	var i =1;
	function addInput(){
		var container = document.createElement('container');
    	container.innerHTML = '<div class="row" ><div class="form-group"><div class="col-md-2 control-label"><label class="control-label">Data Balikan '+i+'</label></div><div class="col-md-4"><input type="text" name="import_file[]" class="form-control" /></div></div></div>';
    	document.getElementById('place').appendChild(container);
    	i++;
	}     
</script>



