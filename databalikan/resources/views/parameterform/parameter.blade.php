@extends('layouts.template')
@section('title', 'Parameter')
@section('content')
<section id="section1" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h4 class="page-title">@yield('title')</h4> </div>
    <!-- /.col-lg-12 -->
	</div>
<!--START AREA ATAS-->
<div class="row">
<div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading">@yield('title')</div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div class="panel-body">
                <form action="#" class="form-horizontal">
                    <div class="form-body">
                        <!--<h3 class="box-title">Person Info</h3>
                        <hr>-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Test</label>
                                    <input type="text" class="form-control" />
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Test</label>
                                    <select name="select" class="form-control">
                                    </select>
                                    </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"></label> </div>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn btn-default">Back</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
 <!--END AREA ATAS-->
<!--START AREA BAWAH-->
<!--<div class="row">
    <div class="col-md-12">
        <div class="white-box">
                    
        </div>
     </div>
</div>-->
<!--END AREA BAWAH-->
</div>
</section>
@stop