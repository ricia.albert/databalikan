@extends('layouts.template')
@section('title', 'Detail Data')
@section('content')
<style>
th {
    color:#31708f;background-color:#d9edf7;border-color:#bce8f1;
	font-size:18px;font-weight:500;
} 
</style>
<section id="section2" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h3 class="page-title">&nbsp;</h3> </div>
    <!-- /.col-lg-12 -->
	</div>
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h3 class="page-title">&nbsp;</h3> </div>
    <!-- /.col-lg-12 -->
	</div>
<!--START AREA ATAS-->
@foreach($detail->content as $result)
@php 
   $date = new DateTime($result->TGL_LHR);
   $tgl_lahir = $date->format('d-m-Y');
   
   $date_ektp = new DateTime($result->EKTP_CREATED);
   $tgl_ektp = $date_ektp->format('d-m-Y');
   
    $image = base64_decode($result->FOTO);
    $image_name = $result->NIK.".jpg";
    $path = public_path() . "/images/" . $image_name;
    
    file_put_contents($path, $image);
@endphp
<div class="row">
<div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading text-center" style="font-size:24px; font-weight:500;">PROPINSI {!! $result->PROP_NAME !!}</div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div class="panel-body">
                <form action="#">
                    <div class="form-body">
                        <!--<h3 class="box-title">Person Info</h3>
                        <hr>-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="text-center"><img src="{{ url('public/images/'.$image_name) }}" width="125px" /></div>
                                    <div class="text-center h4">{!! $result->NAMA_LGKP !!}</div>
                                    <div class="text-center h5">{!! $result->NIK !!}</div>
                                 </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-9">
                            	<div class="table-responsive">
                                <table class="table-condensed">
                                	<tbody>
                                      <tr>
                                        <td>Tempat Lahir</td>
                                        <td>:</td>
                                        <td>{!! $result->TMPT_LHR !!}</td>
                                        <td>&nbsp;</td>
                                        <td>No. KK</td>
                                        <td>:</td>
                                        <td>{!! $result->NO_KK !!}</td>
                                      </tr>
                                      <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>{!! $tgl_lahir !!}</td>
                                        <td>&nbsp;</td>
                                        <td width="25%">Status Hub. Keluarga</td>
                                        <td>:</td>
                                        <td>{!! $result->STAT_HBKEL !!}</td>
                                      </tr>
                                      <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>{!! $result->JENIS_KLMIN !!}</td>
                                        <td>&nbsp;</td>
                                        <td>Kewarganegaraan</td>
                                        <td>:</td>
                                        <td>Indonesia</td>
                                      </tr>
                                      <tr>
                                        <td> Alamat </td>
                                        <td>:</td>
                                        <td>{!! $result->ALAMAT !!}</td>
                                        <td>&nbsp;</td>
                                        <td>No. Akta Lahir</td>
                                        <td>:</td>
                                        <td>{!! ($result->NO_AKTA_LHR) ? $result->NO_AKTA_LHR : "-"; !!}</td>
                                      </tr>
                                      <tr>
                                        <td>RT/RW</td>
                                        <td>:</td>
                                        <td>{!! $result->NO_RT !!} / {!! $result->NO_RW !!}</td>
                                        <td>&nbsp;</td>
                                        <td>Status Kawin</td>
                                        <td>:</td>
                                        <td>{!! $result->STATUS_KAWIN !!}</td>
                                      </tr>
                                      <tr>
                                        <td>Kelurahan/Desa</td>
                                        <td>:</td>
                                        <td>{!! ($result->KEL_NAME) ? $result->KEL_NAME : "-"; !!}</td>
                                        <td>&nbsp;</td>
                                        <td>Tanggal Kawin</td>
                                        <td>:</td>
                                        <td>{!! ($result->TGL_KWN) ? $result->TGL_KWN : "-"; !!}</td>
                                      </tr>
                                      <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{!! $result->KEC_NAME !!}</td>
                                        <td>&nbsp;</td>
                                        <td>Tgl Rekam EKTP</td>
                                        <td>:</td>
                                        <td>{!! $tgl_ektp !!}</td>
                                      </tr>
                                      <tr>
                                        <td>Kabupaten</td>
                                        <td>:</td>
                                        <td>{!! $result->KAB_NAME !!}</td>
                                        <td>&nbsp;</td>
                                        <td>Pekerjaan</td>
                                        <td>:</td>
                                        <td>{!! $result->JENIS_PKRJN !!}</td>
                                      </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!--/span-->
                        </div>                        
                    </div>
                    <div class="pull-right">
                    	<button type="button" onclick="document.location.replace('{{ url('listdata') }}')" class="btn btn-default">Kembali</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
@endforeach
 <!--END AREA ATAS-->
<!--START AREA BAWAH-->
@if(count($hasil)>0)
<div class="row">
<div class="col-lg-12">
<div class="table-responsive">        
    <table id="myTable" class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center text-uppercase">No.</th>
            <th class="text-center text-uppercase">Nama Lembaga</th>
            <th class="text-center text-uppercase">Data Balikan</th>
          </tr>
        </thead>
        <tbody>
            @for($i=0;$i< count($hasil);$i++)
            <tr>
                <td class="text-center">{!! $i+1 !!}</td>
                <td>{!! $hasil[$i]->lembaga_pengguna !!}</td>
                <td>
                @foreach($lembaga[$i] as $item=>$itemval)
                @if ($itemval->parameter=="NIK")
                @else
                	{!! $itemval->parameter !!} : {!! $itemval->value !!}<br />
                @endif
                @endforeach
                </td>
            </tr>
            @endfor	
        </tbody>
    </table>
</div>              
</div>
</div>
@endif
<!--END AREA BAWAH-->
</div>
</section>
@stop