@extends('layouts.template')
@section('title', 'Parameter')
@section('content')
<section id="section1" class="container">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    	<h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h3 class="page-title">&nbsp;</h3> </div>
        <h4 class="page-title">Form Input Data Balikan</h4> </div>
    <!-- /.col-lg-12 -->
	</div>
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-info">
	        <div class="panel-heading"><p style="font-size: x-large;">{{$nama}}</p></div>
	        <div class="panel-wrapper collapse in" aria-expanded="true">
	            <div class="panel-body">
	                <form action="{{ URL::to('formPost') }}" class="form-horizontal" method="post">
	                    <div class="form-body">
	                        <!--<h3 class="box-title">Person Info</h3>
	                        <hr>-->
	                        {{ csrf_field() }}
	                        <div id="place">
	                        	@if ($message != '')
	                        	<div class="alert alert-success" role="alert">{{ $message }}</div>
	                        	@endif
		                        @foreach ($prevs as $prev)
		                        <div class="row" >
		                                <div class="form-group">
		                                <div class="col-md-2 control-label">
		                                    <label class="control-label">{{$prev->parameter}}</label>
		                                </div>
		    							<div class="col-md-4">
			                                    <input type="text" name="field[{{$prev->parameter}}]" class="form-control" />
			                            	</div>
		                            	</div>
								</div>	
								@endforeach
							</div>
	                            
	                        <!--/row-->
	                    </div>
	                    <div class="form-actions">
	                        <div class="col-md-2"></div>
	                        <div class="col-md-4">
	                            <button class="btn btn-primary">submit</button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
</div>

</div>
</section>
@stop



